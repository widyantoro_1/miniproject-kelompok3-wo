package id.spring_reactjs.kelompok_miniproject.assembler;

import id.spring_reactjs.kelompok_miniproject.model.dto.AssignmentDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Assignment;
import id.spring_reactjs.kelompok_miniproject.model.entity.Staff;
import id.spring_reactjs.kelompok_miniproject.model.entity.User;
import id.spring_reactjs.kelompok_miniproject.repository.AssignmentRepository;
import id.spring_reactjs.kelompok_miniproject.repository.StaffRepository;
import id.spring_reactjs.kelompok_miniproject.repository.UserRepository;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AssignmentAssembler implements InterfaceAssembler<Assignment, AssignmentDto>{

	@Autowired
	AssignmentRepository assignmentRepository;
	
	@Autowired
	StaffRepository staffRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Override
	public Assignment fromDto(AssignmentDto dto) {
		
		Assignment entity = new Assignment();
		if (dto.getIdAssignment() != null) {
			Optional<Assignment> temp = this.assignmentRepository.findById(dto.getIdAssignment());
			if (temp.isPresent()) {
				entity = temp.get();
			}
		}
		
		if (dto.getIdStaff() != null) {
			Staff entityStaff = staffRepository.findById(dto.getIdStaff()).get();
			entity.setStaff(entityStaff);
			entity.setNameStaff(entityStaff.getNameStaff());
			entity.setJob(entityStaff.getJob());
		}
		if (dto.getIdUser() != null) {
			User entityUser = userRepository.findById(dto.getIdUser()).get();
			entity.setUser(entityUser);
			entity.setStaffSchedule(entityUser.getWeddingDate());
		}
		
		return entity;
	}
	
	@Override
	public AssignmentDto fromEntity(Assignment entity) {
		if (entity == null) return null;
		return AssignmentDto.builder()
				.idAssignment(entity.getIdAssignment())
				.idStaff(entity.getStaff().getIdStaff())
				.nameStaff(entity.getNameStaff())
				.job(entity.getJob())
				.idUser(entity.getUser().getIdUser())
				.staffSchedule(entity.getStaffSchedule())
				.build();
	}
	
}
