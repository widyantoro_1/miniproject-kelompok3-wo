package id.spring_reactjs.kelompok_miniproject.assembler;

import org.springframework.stereotype.Component;

import id.spring_reactjs.kelompok_miniproject.model.dto.DamDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Dam;

@Component
public class DamAssembler implements InterfaceAssembler<Dam, DamDto> {

    
    @Override
    public Dam fromDto(DamDto dto) {
        if (dto == null)
            return null;
        
        Dam entity = new Dam();
        if (dto.getIdDam() != null) entity.setIdDam(dto.getIdDam());
        if (dto.getNameDam() != null) entity.setNameDam(dto.getNameDam());
        if (dto.getStyleDam() != null) entity.setStyleDam(dto.getStyleDam());
        if (dto.getImageDam() != null) entity.setImageDam(dto.getImageDam());
        if (dto.getPriceDam() != null) entity.setPriceDam(dto.getPriceDam());
        if (dto.getDetailDam() != null) entity.setDetailDam(dto.getDetailDam());
        
        return entity;
    }
    
    @Override
    public DamDto fromEntity(Dam entity) {
        if(entity == null) return null;
        return DamDto.builder()
        		.idDam(entity.getIdDam())
                .nameDam(entity.getNameDam())
                .styleDam(entity.getStyleDam())
                .imageDam(entity.getImageDam())
                .priceDam(entity.getPriceDam())
                .detailDam(entity.getDetailDam())
                .build();
	}
}
