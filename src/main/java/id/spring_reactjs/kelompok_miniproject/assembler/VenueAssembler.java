package id.spring_reactjs.kelompok_miniproject.assembler;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.spring_reactjs.kelompok_miniproject.assembler.InterfaceAssembler;
import id.spring_reactjs.kelompok_miniproject.model.dto.VenueDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Venue;
import id.spring_reactjs.kelompok_miniproject.repository.VenueRepository;

import java.util.Optional;

@Component
public class VenueAssembler implements InterfaceAssembler<Venue, VenueDto> {

    @Autowired
    private VenueRepository repository;

    @Override
    public Venue fromDto(VenueDto dto) {
        if (dto == null)
            return null;

        Venue entity = new Venue();
//        if (dto.getCode() != null) {
//            Optional<VenueEntity> temp = this.repository.findById(dto.getCode());
//            if(temp.isPresent()){
//                entity = temp.get();
//            }
//        }

        if(dto.getIdVenue() != null) entity.setIdVenue(dto.getIdVenue());
        if(dto.getNameVenue() != null) entity.setNameVenue(dto.getNameVenue());
        if(dto.getLocation() != null) entity.setLocation(dto.getLocation());
        if(dto.getAmbiance() != null) entity.setAmbiance(dto.getAmbiance());
        if(dto.getCapacity() != null) entity.setCapacity(dto.getCapacity());
        if(dto.getImageVenue() != null) entity.setImageVenue(dto.getImageVenue());
        if(dto.getPriceVenue() != null) entity.setPriceVenue(dto.getPriceVenue());
        if(dto.getDetailVenue() != null) entity.setDetailVenue(dto.getDetailVenue());

        return entity;
	}

	@Override
	public VenueDto fromEntity(Venue entity) {
		if (entity == null)
			return null;
		return VenueDto.builder()
				.idVenue(entity.getIdVenue())
				.nameVenue(entity.getNameVenue())
				.location(entity.getLocation())
				.ambiance(entity.getAmbiance())
				.capacity(entity.getCapacity())
				.imageVenue(entity.getImageVenue())
				.priceVenue(entity.getPriceVenue())
				.detailVenue(entity.getDetailVenue()).build();
	}	

}
