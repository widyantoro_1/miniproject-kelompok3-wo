package id.spring_reactjs.kelompok_miniproject.assembler;

import id.spring_reactjs.kelompok_miniproject.model.dto.CateringDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Catering;
import id.spring_reactjs.kelompok_miniproject.repository.CateringRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CateringAssembler implements InterfaceAssembler<Catering, CateringDto> {

	@Autowired
	private CateringRepository cateringRepository;

	@Override
	public Catering fromDto(CateringDto dto) {
		if (dto == null)
			return null;

		Catering entity = new Catering();
		if (dto.getIdCatering() != null) {
			Optional<Catering> temp = this.cateringRepository.findById(dto.getIdCatering());
			if (temp.isPresent()) {
				entity = temp.get();
			}
		}

		if (dto.getIdCatering() != null)
			entity.setIdCatering(dto.getIdCatering());
		if (dto.getNameCatering() != null)
			entity.setNameCatering(dto.getNameCatering());
		if (dto.getStyleCatering() != null)
			entity.setStyleCatering(dto.getStyleCatering());
		if (dto.getPortionCatering() != null)
			entity.setPortionCatering(dto.getPortionCatering());
		if (dto.getImageCatering() != null)
			entity.setImageCatering(dto.getImageCatering());
		if (dto.getPriceCatering() != null)
			entity.setPriceCatering(dto.getPriceCatering());
		if (dto.getDetailCatering() != null)
			entity.setDetailCatering(dto.getDetailCatering());

		return entity;
	}

	@Override
	public CateringDto fromEntity(Catering entity) {
		if (entity == null)
			return null;
		return CateringDto.builder().idCatering(entity.getIdCatering()).nameCatering(entity.getNameCatering())
				.styleCatering(entity.getStyleCatering()).portionCatering(entity.getPortionCatering())
				.imageCatering(entity.getImageCatering()).priceCatering(entity.getPriceCatering())
				.detailCatering(entity.getDetailCatering()).build();
	}
}
