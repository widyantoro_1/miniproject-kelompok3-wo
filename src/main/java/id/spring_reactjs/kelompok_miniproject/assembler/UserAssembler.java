package id.spring_reactjs.kelompok_miniproject.assembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.spring_reactjs.kelompok_miniproject.model.dto.UserDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.User;
import id.spring_reactjs.kelompok_miniproject.repository.CateringRepository;
import id.spring_reactjs.kelompok_miniproject.repository.DamRepository;
import id.spring_reactjs.kelompok_miniproject.repository.DecorRepository;
import id.spring_reactjs.kelompok_miniproject.repository.PhotoRepository;
import id.spring_reactjs.kelompok_miniproject.repository.VenueRepository;


@Component
public class UserAssembler implements InterfaceAssembler<User, UserDto> {

	@Autowired
	private VenueRepository venueRepository;

	@Autowired
	private DamRepository damRepository;

	@Autowired
	private DecorRepository decorRepository;

	@Autowired
	private CateringRepository cateringRepository;

	@Autowired
	private PhotoRepository photoRepository;

	@Override
	public User fromDto(UserDto dto) {
		if (dto == null)
			return null;

		Integer priceVenue = 0, priceDecor = 0, priceDam = 0, priceCatering = 0, pricePhoto = 0;
		Integer venue = 0, decor = 0, dam = 0, catering = 0, photo = 0;

		User entity = new User();
		if (dto.getIdUser() != null) {
			entity.setIdUser(dto.getIdUser());
		}
		if (dto.getNameBride() != null) {
			entity.setNameBride(dto.getNameBride());
		}
		if (dto.getNameGroom() != null) {
			entity.setNameGroom(dto.getNameGroom());
		}
		if (dto.getEmail() != null) {
			entity.setEmail(dto.getEmail());
		}
		if (dto.getNoHp() != null) {
			entity.setNoHp(dto.getNoHp());
		}
		if (dto.getWeddingDate() != null) {
			entity.setWeddingDate(dto.getWeddingDate());
		}
		if (dto.getGuestNumber() != null) {
			entity.setGuestNumber(dto.getGuestNumber());
		}
		if (dto.getIdVenue() != null) {
			entity.setIdVenue(dto.getIdVenue());
			priceVenue = venueRepository.findByIdVenue(dto.getIdVenue()).getPriceVenue();
			venue = 1;
		}
		if (dto.getIdDecor() != null) {
			entity.setIdDecor(dto.getIdDecor());
			priceDecor = decorRepository.findAllByIdDecor(dto.getIdDecor()).getPriceDecor();
			decor = 1;
		}
		if (dto.getIdDam() != null) {
			entity.setIdDam(dto.getIdDam());
			priceDam = damRepository.findByIdDam(dto.getIdDam()).getPriceDam();
			dam = 1;
		}
		if (dto.getIdCatering() != null) {
			entity.setIdCatering(dto.getIdCatering());
			priceCatering = cateringRepository.findAllByIdCatering(dto.getIdCatering()).getPriceCatering();
			catering = 1;
		}
		if (dto.getIdPhoto() != null) {
			entity.setIdPhoto(dto.getIdPhoto());
			pricePhoto = photoRepository.findByIdPhoto(dto.getIdPhoto()).getPricePhoto();
			photo = 1;
		}
//		if(dto.getNameVenue()!=null) entity.setVenue(venueRepository.findByNameVenue(dto.getNameVenue()));
//		if(dto.getNameDecor()!=null) entity.setDecor(decorRepository.findByNameDecor(dto.getNameDecor()));
//		if(dto.getNameDam()!=null) entity.setDam(damRepository.findByNameDam(dto.getNameDam()));
//		if(dto.getNameCatering()!=null) entity.setCatering(cateringRepository.getByName(dto.getNameCatering()));
//		if(dto.getNamePhoto()!=null) entity.setPhoto(photoRepository.findByNamePhoto(dto.getNamePhoto()));

		Integer totalPrice = priceVenue + priceDecor + priceDam + priceCatering + pricePhoto;
		entity.setTotalPrice(totalPrice);
		Integer countProduct = venue + decor + dam + catering + photo;
		entity.setCountProduct(countProduct);
		if (dto.getReference() != null) {
			entity.setReference(dto.getReference());
		}
		if (dto.getProgress() != null) {
			entity.setProgress(dto.getProgress());
		}
		if (dto.getUpdatedDate() != null) {
			entity.setUpdatedDate(dto.getUpdatedDate());
		}
		if (dto.getAdmin() != null) {
			entity.setAdmin(dto.getAdmin());
		}
		return entity;
	}

	@Override
	public UserDto fromEntity(User entity) {
		if (entity == null)
			return null;
		Integer priceVenue = 0, priceDecor = 0, priceDam = 0, priceCatering = 0, pricePhoto = 0;
		if (entity.getIdVenue() != null) {
			priceVenue = venueRepository.findByIdVenue(entity.getIdVenue()).getPriceVenue();
		}
		if (entity.getIdDecor() != null) {
			priceDecor = decorRepository.findAllByIdDecor(entity.getIdDecor()).getPriceDecor();
		}
		if (entity.getIdDam() != null) {
			priceDam = damRepository.findByIdDam(entity.getIdDam()).getPriceDam();
		}
		if (entity.getIdCatering() != null) {
			priceCatering = cateringRepository.findAllByIdCatering(entity.getIdCatering()).getPriceCatering();
		}
		if (entity.getIdPhoto() != null) {
			pricePhoto = photoRepository.findByIdPhoto(entity.getIdPhoto()).getPricePhoto();
		}

		String venue = null, decor = null, dam = null, catering = null, photo = null;
		if (entity.getIdVenue() != null) {
			venue = venueRepository.findByIdVenue(entity.getIdVenue()).getNameVenue();
		}
		if (entity.getIdDecor() != null) {
			decor = decorRepository.findAllByIdDecor(entity.getIdDecor()).getNameDecor();
		}
		if (entity.getIdDam() != null) {
			dam = damRepository.findByIdDam(entity.getIdDam()).getNameDam();
		}
		if (entity.getIdCatering() != null) {
			catering = cateringRepository.findAllByIdCatering(entity.getIdCatering()).getNameCatering();
		}
		if (entity.getIdPhoto() != null) {
			photo = photoRepository.findByIdPhoto(entity.getIdPhoto()).getNamePhoto();
		}

		Integer totalPrice = priceVenue + priceDecor + priceDam + priceCatering + pricePhoto;

		return UserDto.builder().idUser(entity.getIdUser()).nameBride(entity.getNameBride())
				.nameGroom(entity.getNameGroom()).email(entity.getEmail()).noHp(entity.getNoHp())
				.weddingDate(entity.getWeddingDate()).guestNumber(entity.getGuestNumber()).idVenue(entity.getIdVenue())
				.idDecor(entity.getIdDecor()).idDam(entity.getIdDam()).idCatering(entity.getIdCatering())
				.idPhoto(entity.getIdPhoto()).nameVenue(venue).nameDecor(decor).nameDam(dam).nameCatering(catering)
				.namePhoto(photo)
//				.nameVenue(entity.getVenue().getNameVenue())
//				.nameDecor(entity.getDecor().getNameDecor())
//				.nameDam(entity.getDam().getNameDam())
//				.nameCatering(entity.getCatering().getNameCatering())
//				.namePhoto(entity.getPhoto().getNamePhoto())
				.totalPrice(totalPrice).countProduct(entity.getCountProduct()).reference(entity.getReference())
				.progress(entity.getProgress()).updatedDate(entity.getUpdatedDate()).admin(entity.getAdmin()).build();
	}

}
