package id.spring_reactjs.kelompok_miniproject.assembler;

import org.springframework.stereotype.Component;

import id.spring_reactjs.kelompok_miniproject.model.dto.StaffDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Staff;

@Component
public class StaffAssembler implements InterfaceAssembler<Staff, StaffDto> {
	

	@Override
	public Staff fromDto(StaffDto dto) {
		if (dto == null)
			return null;

		Staff entity = new Staff();
//		if (dto.getIdStaff() != null) {
//			Optional<Staff> temp = this.staffRepository.findById(dto.getIdStaff());
//			if (temp.isPresent()) {
//				entity=temp.get();
//			}
//		}
		
		if(dto.getIdStaff()!= null) entity.setIdStaff(dto.getIdStaff());
		if(dto.getNameStaff()!= null) entity.setNameStaff(dto.getNameStaff());
		if(dto.getJob()!=null) entity.setJob(dto.getJob());
		if(dto.getProfile()!=null) entity.setProfile(dto.getProfile());
		if(dto.getStaffSchedule()!=null) entity.setStaffSchedule(dto.getStaffSchedule());

		return entity;
	}

	@Override
	public StaffDto fromEntity(Staff entity) {
		if(entity==null) return null;
		return StaffDto.builder()
				.idStaff(entity.getIdStaff())
				.nameStaff(entity.getNameStaff())
				.job(entity.getJob())
				.profile(entity.getProfile())
				.staffSchedule(entity.getStaffSchedule())
				.build();
	}

}
