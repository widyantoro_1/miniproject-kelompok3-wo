package id.spring_reactjs.kelompok_miniproject.assembler;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.spring_reactjs.kelompok_miniproject.model.dto.DecorDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Decor;
import id.spring_reactjs.kelompok_miniproject.repository.DecorRepository;

@Component
public class DecorAssembler implements InterfaceAssembler<Decor, DecorDto> {


	@Autowired
	private DecorRepository decorRepository;
	@Override
	public Decor fromDto(DecorDto dto) {
		if (dto == null)
			return null;
		
		Decor decorEntity = new Decor();
		if (dto.getIdDecor() != null) {
			Optional<Decor> temp = this.decorRepository.findById(dto.getIdDecor());
			if (temp.isPresent()) {
				decorEntity=temp.get();
			}
		}
		if (dto.getIdDecor() != null) decorEntity.setIdDecor(dto.getIdDecor());
		if (dto.getImageDecor() != null) decorEntity.setImageDecor(dto.getImageDecor());
		if (dto.getNameDecor() != null) decorEntity.setNameDecor(dto.getNameDecor());
		if (dto.getStyleDecor() != null) decorEntity.setStyleDecor(dto.getStyleDecor());
		if (dto.getPriceDecor() != null) decorEntity.setPriceDecor(dto.getPriceDecor());
		if (dto.getDetailDecor() != null) decorEntity.setDetailDecor(dto.getDetailDecor());

		return decorEntity;
	}

	@Override
	public DecorDto fromEntity(Decor entity) {
		if(entity == null) return null;
		return DecorDto.builder()
				.idDecor(entity.getIdDecor())
				.nameDecor(entity.getNameDecor())
				.styleDecor(entity.getStyleDecor())
				.imageDecor(entity.getImageDecor())
				.priceDecor(entity.getPriceDecor())
				.detailDecor(entity.getDetailDecor())
				.build();
	}
}
