package id.spring_reactjs.kelompok_miniproject.assembler;
import org.springframework.stereotype.Component;

import id.spring_reactjs.kelompok_miniproject.model.dto.PhotoDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Photo;

@Component
public class PhotoAssembler implements InterfaceAssembler<Photo, PhotoDto> {

	@Override
	public Photo fromDto(PhotoDto dto) {
		if (dto == null)
			return null;

		Photo entity = new Photo();
//		if (dto.getIdPhoto() != null) {
//			Optional<Photo> temp = this.photoRepository.findById(dto.getIdPhoto());
//			if (temp.isPresent()) {
//				entity=temp.get();
//			}
//		}
		
		if(dto.getIdPhoto()!= null) entity.setIdPhoto(dto.getIdPhoto());
		if(dto.getNamePhoto()!= null) entity.setNamePhoto(dto.getNamePhoto());
		if(dto.getImagePhoto()!=null) entity.setImagePhoto(dto.getImagePhoto());
		if(dto.getThemePhoto()!=null) entity.setThemePhoto(dto.getThemePhoto());
		if(dto.getPricePhoto()!=null) entity.setPricePhoto(dto.getPricePhoto());
		if(dto.getDetailPhoto()!=null) entity.setDetailPhoto(dto.getDetailPhoto());

		return entity;
	}

	@Override
	public PhotoDto fromEntity(Photo entity) {
		if(entity==null) return null;
		return PhotoDto.builder()
				.idPhoto(entity.getIdPhoto())
				.namePhoto(entity.getNamePhoto())
				.imagePhoto(entity.getImagePhoto())
				.themePhoto(entity.getThemePhoto())
				.pricePhoto(entity.getPricePhoto())
				.detailPhoto(entity.getDetailPhoto())
				.build();
	}

}
