package id.spring_reactjs.kelompok_miniproject.assembler;

import org.springframework.stereotype.Component;

import id.spring_reactjs.kelompok_miniproject.model.dto.AdminDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Admin;

@Component
public class AdminAssembler implements InterfaceAssembler<Admin, AdminDto> {


	@Override
	public Admin fromDto(AdminDto dto) {
		if (dto == null)
			return null;

		Admin entity = new Admin();

		if (dto.getIdAdmin() != null)
			entity.setIdAdmin(dto.getIdAdmin());
		if (dto.getUnameAdmin() != null)
			entity.setUnameAdmin(dto.getUnameAdmin());
		if (dto.getNameAdmin() != null)
			entity.setNameAdmin(dto.getNameAdmin());
		if (dto.getPassAdmin() != null)
			entity.setPassAdmin(dto.getPassAdmin());
		if (dto.getImageAdmin() != null)
			entity.setImageAdmin(dto.getImageAdmin());
		
		return entity;
	}
	
	@Override
	public AdminDto fromEntity(Admin entity) {
		if(entity == null)
			return null;
		return AdminDto.builder()
				.idAdmin(entity.getIdAdmin())
				.unameAdmin(entity.getUnameAdmin())
				.passAdmin(entity.getPassAdmin())
				.imageAdmin(entity.getImageAdmin())
				.nameAdmin(entity.getNameAdmin())
				.build();
		
	}

}
