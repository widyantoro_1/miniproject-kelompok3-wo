package id.spring_reactjs.kelompok_miniproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.spring_reactjs.kelompok_miniproject.assembler.UserAssembler;
import id.spring_reactjs.kelompok_miniproject.model.dto.UserDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.User;
import id.spring_reactjs.kelompok_miniproject.repository.UserRepository;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository repository;

	@Autowired
	private UserAssembler bookedlistAssembler;

	@Override
	public UserDto insertBookedlist(UserDto dto) {
		User entity = repository.save(bookedlistAssembler.fromDto(dto));
		repository.save(entity);
		bookedlistAssembler.fromEntity(entity);
		return bookedlistAssembler.fromEntity(entity);
	}

	@Override
	public UserDto editBookedlist(User user) {
		User entity = repository.save(user);
		repository.save(entity);
		bookedlistAssembler.fromEntity(entity);
		return bookedlistAssembler.fromEntity(entity);
	}

}