package id.spring_reactjs.kelompok_miniproject.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import id.spring_reactjs.kelompok_miniproject.model.dto.AdminDto;

public interface AdminService {
	AdminDto insertDataAdmin(AdminDto dto);

	void storeFile(MultipartFile file) throws IOException;

}
