package id.spring_reactjs.kelompok_miniproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.spring_reactjs.kelompok_miniproject.assembler.StaffAssembler;
import id.spring_reactjs.kelompok_miniproject.model.dto.StaffDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Staff;
import id.spring_reactjs.kelompok_miniproject.repository.StaffRepository;

@Service
@Transactional
public class StaffServiceImpl implements StaffService {
	@Autowired
	private StaffRepository staffRepository;
	@Autowired
	private StaffAssembler staffAssembler;

	@Override
	public StaffDto insertStaff(StaffDto dto) {
		Staff entity = staffRepository.save(staffAssembler.fromDto(dto));
		staffRepository.save(entity);
		return staffAssembler.fromEntity(entity);
	}

}
