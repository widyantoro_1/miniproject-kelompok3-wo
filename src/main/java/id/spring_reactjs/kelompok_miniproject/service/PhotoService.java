package id.spring_reactjs.kelompok_miniproject.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import id.spring_reactjs.kelompok_miniproject.model.dto.PhotoDto;

public interface PhotoService {
	PhotoDto insertPhotoVideo(PhotoDto dto);
//	PhotoDto insertUpload(PhotoDto dto);
//	void upload(MultipartFile file) throws IllegalStateException, IOException;
}
