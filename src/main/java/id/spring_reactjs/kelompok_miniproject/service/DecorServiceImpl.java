package id.spring_reactjs.kelompok_miniproject.service;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import antlr.StringUtils;
import id.spring_reactjs.kelompok_miniproject.assembler.DecorAssembler;
import id.spring_reactjs.kelompok_miniproject.model.dto.CateringDto;
import id.spring_reactjs.kelompok_miniproject.model.dto.DecorDto;
import id.spring_reactjs.kelompok_miniproject.model.dto.PhotoDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Catering;
import id.spring_reactjs.kelompok_miniproject.model.entity.Decor;
import id.spring_reactjs.kelompok_miniproject.model.entity.Photo;
import id.spring_reactjs.kelompok_miniproject.repository.DecorRepository;

@Service
@Transactional
public class DecorServiceImpl implements DecorService{
	@Autowired
	private DecorRepository decorRepository;
	@Autowired
	private DecorAssembler decorAssembler;
	
	@Override
	public DecorDto insertDecorMulti(DecorDto dto, String url) {
		Decor entity = decorRepository.save(decorAssembler.fromDto(dto));
		decorRepository.save(entity);
		entity.setImageDecor(url);
		decorRepository.save(entity);
		return decorAssembler.fromEntity(entity);
	}
	@Override
	public DecorDto insertDecor(DecorDto dto) {
		Decor entity = decorRepository.save(decorAssembler.fromDto(dto));
		decorRepository.save(entity);
		return decorAssembler.fromEntity(entity);
	}
	
	@Override
	public void uploadFile(MultipartFile file) throws IllegalStateException, IOException{
		file.transferTo(new File("C:/Users/lenovo/Documents/react-wedding-organizer2/kerinci-stories/src/img/ImgDecor/"+file.getOriginalFilename()));
	}
	
	
	
}
