package id.spring_reactjs.kelompok_miniproject.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import id.spring_reactjs.kelompok_miniproject.model.dto.CateringDto;

public interface CateringService {

	CateringDto insertCatering(CateringDto dto);
	CateringDto insertCateringUpload(CateringDto dto);
	void upload(MultipartFile file) throws IllegalStateException, IOException;
}
