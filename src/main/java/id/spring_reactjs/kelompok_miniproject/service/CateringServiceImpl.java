package id.spring_reactjs.kelompok_miniproject.service;

import id.spring_reactjs.kelompok_miniproject.assembler.CateringAssembler;
import id.spring_reactjs.kelompok_miniproject.model.dto.CateringDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Catering;
import id.spring_reactjs.kelompok_miniproject.repository.CateringRepository;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
@Transactional
public class CateringServiceImpl implements CateringService {
	@Autowired
	private CateringRepository cateringRepository;
	
	@Autowired
	private CateringAssembler cateringAssembler;
	
	@Override
	public CateringDto insertCatering(CateringDto dto) {
		Catering entity = cateringRepository.save(cateringAssembler.fromDto(dto));
		cateringRepository.save(entity);
		return cateringAssembler.fromEntity(entity);
	}
	
	@Override
	public CateringDto insertCateringUpload(CateringDto dto) {
		Catering entity = cateringRepository.save(cateringAssembler.fromDto(dto));
		cateringRepository.save(entity);
		return cateringAssembler.fromEntity(entity);
	}
	
	@Override
	public void upload(MultipartFile file) throws IllegalStateException, IOException{
		file.transferTo(new File("C:\\Users\\lenovo\\Documents\\react-wedding-organizer\\kerinci-stories\\public\\images\\"+file.getOriginalFilename()));
	}

}
