package id.spring_reactjs.kelompok_miniproject.service;

import java.io.File;
import java.io.IOException;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import id.spring_reactjs.kelompok_miniproject.assembler.AdminAssembler;
import id.spring_reactjs.kelompok_miniproject.model.dto.AdminDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Admin;
import id.spring_reactjs.kelompok_miniproject.repository.AdminRepository;

@Service
@Transactional
public class AdminServiceImpl implements AdminService{
	@Autowired
	private AdminRepository repository;
	@Autowired
	private AdminAssembler assembler;

	@Override
	public AdminDto insertDataAdmin(AdminDto dto) {
		Admin entity = repository.save(assembler.fromDto(dto));
		repository.save(entity);
		return assembler.fromEntity(entity);
	}

	@Override
	public void storeFile(MultipartFile file) throws IOException {
//		file.transferTo(new File(
//				"/home/prosigmaka/Documents/reactjs_project/frontend-react-weddingorganizer/react-wedding-organizer/kerinci-stories/src/img/ImgAdmin"
//						+ file.getOriginalFilename()));
//		file.transferTo(new File("/home/prosigmaka/Documents/img/venue/" + file.getOriginalFilename()));
		file.transferTo(new File("C:\\Users\\lenovo\\Documents\\img-photo" + file.getOriginalFilename()));
	}
	

}
