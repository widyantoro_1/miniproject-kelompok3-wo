package id.spring_reactjs.kelompok_miniproject.service;

import id.spring_reactjs.kelompok_miniproject.model.dto.AssignmentDto;

public interface AssignmentService {
	
	AssignmentDto insertAssignment(AssignmentDto dto);

}
