package id.spring_reactjs.kelompok_miniproject.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import id.spring_reactjs.kelompok_miniproject.model.dto.VenueDto;

public interface VenueService {
	VenueDto insertDataVenue(VenueDto dto);

	void storeFile(MultipartFile file) throws IOException;

	//void deleteVenueImage(String url);

}
