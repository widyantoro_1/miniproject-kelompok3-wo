package id.spring_reactjs.kelompok_miniproject.service;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.spring_reactjs.kelompok_miniproject.assembler.DamAssembler;
import id.spring_reactjs.kelompok_miniproject.model.dto.DamDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Dam;
import id.spring_reactjs.kelompok_miniproject.repository.DamRepository;

@Service
@Transactional
public class DamServiceImpl implements DamService {

	@Autowired
	private DamRepository damRepository;

	@Autowired
	private DamAssembler damAssembler;

    @Override
	public DamDto insertDam (DamDto dto) {
		Dam entity = damRepository.save(damAssembler.fromDto(dto));
		damRepository.save(entity);
		return damAssembler.fromEntity(entity);
	}

//	@Override
//	public DamDto insertDam(DamDto dto, String url) {
//		Dam entity = damRepository.save(damAssembler.fromDto(dto));
//		entity.setImageDam(url);
//		damRepository.save(entity);
//		return damAssembler.fromEntity(entity);
//	}
    
//    @Override
//    public void uploadDam(MultipartFile file) throws IllegalStateException, IOException{
//    	file.transferTo(new File("C:/Users/Lenovo/Documents/react-wedding-organizer/kerinci-stories/src/img/ImgDam/"+file.getOriginalFilename()));
//    }
}
