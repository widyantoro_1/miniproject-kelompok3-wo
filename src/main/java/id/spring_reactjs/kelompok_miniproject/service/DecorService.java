package id.spring_reactjs.kelompok_miniproject.service;



import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;

import id.spring_reactjs.kelompok_miniproject.model.dto.DecorDto;
import id.spring_reactjs.kelompok_miniproject.model.dto.PhotoDto;

public interface DecorService {

	DecorDto insertDecor(DecorDto dto);
	DecorDto insertDecorMulti(DecorDto dto, String url);
	void uploadFile(MultipartFile file) throws IllegalStateException, IOException;


	
	
	
}
