package id.spring_reactjs.kelompok_miniproject.service;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.spring_reactjs.kelompok_miniproject.assembler.PhotoAssembler;
import id.spring_reactjs.kelompok_miniproject.model.dto.PhotoDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Photo;
import id.spring_reactjs.kelompok_miniproject.repository.PhotoRepository;

@Service
@Transactional
public class PhotoServiceImpl implements PhotoService {
	@Autowired
	private PhotoRepository photoRepository;

	@Autowired
	private PhotoAssembler photoAssembler;

	@Override
	public PhotoDto insertPhotoVideo(PhotoDto dto) {
		Photo entity = photoRepository.save(photoAssembler.fromDto(dto));
		photoRepository.save(entity);
		return photoAssembler.fromEntity(entity);
	}

//	@Override
//	public PhotoDto insertUpload (PhotoDto dto) {
//		Photo entity = photoRepository.save(photoAssembler.fromDto(dto));
//		photoRepository.save(entity);
//		return photoAssembler.fromEntity(entity);
//	}

//	@Override
//	public void upload(MultipartFile file) throws IllegalStateException, IOException {
//		file.transferTo(new File("C:/Users/lenovo/Documents/react-wedding-organizer2/kerinci-stories/src/img/ImgPhoto/"+file.getOriginalFilename()));
//	}

}
