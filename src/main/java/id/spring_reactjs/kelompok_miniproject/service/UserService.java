package id.spring_reactjs.kelompok_miniproject.service;

import id.spring_reactjs.kelompok_miniproject.model.dto.UserDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.User;

public interface UserService {

	UserDto insertBookedlist(UserDto dto);
	
	UserDto editBookedlist(User user);

}
