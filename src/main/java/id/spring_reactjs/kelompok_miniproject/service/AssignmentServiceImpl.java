package id.spring_reactjs.kelompok_miniproject.service;

import id.spring_reactjs.kelompok_miniproject.assembler.AssignmentAssembler;
import id.spring_reactjs.kelompok_miniproject.model.dto.AssignmentDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Assignment;
import id.spring_reactjs.kelompok_miniproject.model.entity.User;
import id.spring_reactjs.kelompok_miniproject.repository.AssignmentRepository;
import id.spring_reactjs.kelompok_miniproject.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AssignmentServiceImpl implements AssignmentService {

	@Autowired
	private AssignmentRepository assignmentRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AssignmentAssembler assignmentAssembler;

	@Override
	public AssignmentDto insertAssignment(AssignmentDto dto) {

		if (dto.getIdAssignment() != null) {
			Assignment entity = assignmentRepository.findById(dto.getIdAssignment()).get();
			if (dto.getIdUser() == entity.getUser().getIdUser()) {
				Assignment sameData = assignmentRepository.save(assignmentAssembler.fromDto(dto));
				assignmentRepository.save(sameData);
				return assignmentAssembler.fromEntity(sameData);
			} else {
				User oldUser = userRepository.findByIdUser(entity.getUser().getIdUser());
				oldUser.setCountProduct(oldUser.getCountProduct() + 1);
				Assignment diffData = assignmentRepository.save(assignmentAssembler.fromDto(dto));
				assignmentRepository.save(diffData);
				User user = userRepository.findByIdUser(diffData.getUser().getIdUser());
				user.setCountProduct(user.getCountProduct() - 1);
				return assignmentAssembler.fromEntity(diffData);
			}

		} else {
			if (!assignmentRepository.findAllByUserIdUser(dto.getIdUser()).isEmpty()) {
				List<Assignment> afilter = assignmentRepository.findAllByUserIdUser(dto.getIdUser());
				List<AssignmentDto> assignmentdto = afilter.stream().map(a -> assignmentAssembler.fromEntity(a))
						.collect(Collectors.toList());
				List<Object> idstaff = assignmentdto.stream().map(a -> a.getIdStaff()).collect(Collectors.toList());
				for (Object id : idstaff) {
					if (dto.getIdStaff() == id) {
						throw new IllegalArgumentException();
					}
				}
				Assignment entity = assignmentRepository.save(assignmentAssembler.fromDto(dto));
				assignmentRepository.save(entity);
				User user = userRepository.findByIdUser(entity.getUser().getIdUser());
				user.setCountProduct(user.getCountProduct() - 1);
				return assignmentAssembler.fromEntity(entity);
			} else {
				Assignment entity = assignmentRepository.save(assignmentAssembler.fromDto(dto));
				assignmentRepository.save(entity);
				User user = userRepository.findByIdUser(entity.getUser().getIdUser());
				user.setCountProduct(user.getCountProduct() - 1);
				return assignmentAssembler.fromEntity(entity);
			}

		}
	}

}
