package id.spring_reactjs.kelompok_miniproject.service;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import id.spring_reactjs.kelompok_miniproject.assembler.VenueAssembler;
import id.spring_reactjs.kelompok_miniproject.model.dto.VenueDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Venue;
import id.spring_reactjs.kelompok_miniproject.repository.VenueRepository;

@Service
@Transactional
public class VenueServiceImpl implements VenueService {
	@Autowired
	private VenueRepository repository;
	@Autowired
	private VenueAssembler assembler;

	@Override
	public VenueDto insertDataVenue(VenueDto dto) {
		Venue entity = repository.save(assembler.fromDto(dto));
		repository.save(entity);
		return assembler.fromEntity(entity);
	}

	@Override
	public void storeFile(MultipartFile file) throws IOException {
//		file.transferTo(new File(
//				"/home/prosigmaka/Documents/reactjs_project/frontend-react-weddingorganizer/react-wedding-organizer/kerinci-stories/src/img/ImgVenue"
//						+ file.getOriginalFilename()));
		file.transferTo(new File("/home/prosigmaka/Documents/img/venue" + file.getOriginalFilename()));
	}

//	@Override
//	public void deleteVenueImage(String url) {
//		File file = new File(url);
//		file.delete();
//	}
}
