package id.spring_reactjs.kelompok_miniproject.controller;

import id.spring_reactjs.kelompok_miniproject.assembler.CateringAssembler;
import id.spring_reactjs.kelompok_miniproject.model.dto.CateringDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Catering;
import id.spring_reactjs.kelompok_miniproject.configuration.DefaultResponse;
import id.spring_reactjs.kelompok_miniproject.repository.CateringRepository;
import id.spring_reactjs.kelompok_miniproject.service.CateringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/catering")
@CrossOrigin(origins = "http://localhost:3000")
public class CateringController {
	@Autowired
	private CateringRepository cateringRepository;

	@Autowired
	private CateringService cateringService;

	@Autowired
	private CateringAssembler cateringAssembler;

	@GetMapping
	public DefaultResponse get() {
		List<Catering> cateringList = cateringRepository.findAll();
		List<CateringDto> cateringDtoList = cateringList.stream()
				.map(catering -> cateringAssembler.fromEntity(catering)).collect(Collectors.toList());
		return DefaultResponse.ok(cateringDtoList);
	}

	@GetMapping("/{idCatering}")
	public DefaultResponse get(@PathVariable Integer idCatering) {
		CateringDto cateringDto = cateringAssembler.fromEntity(cateringRepository.findById(idCatering).get());
		return DefaultResponse.ok(cateringDto);
	}

	@GetMapping("/name/{nameCatering}")
	public DefaultResponse get(@PathVariable String nameCatering) {
		Catering catering = cateringRepository.getByName(nameCatering);
		CateringDto cateringDto = cateringAssembler.fromEntity(catering);
		return DefaultResponse.ok(cateringDto);
	}

	@GetMapping("/orderbypricedesc")
	public DefaultResponse getPriceDesc() {
		List<Catering> cateringList = cateringRepository.findAllByOrderByPriceCateringDesc();
		List<CateringDto> cateringDtoList = cateringList.stream()
				.map(catering -> cateringAssembler.fromEntity(catering)).collect(Collectors.toList());
		return DefaultResponse.ok(cateringDtoList);
	}

	@GetMapping("/orderbypriceasc")
	public DefaultResponse getPriceAsc() {
		List<Catering> cateringList = cateringRepository.findAllByOrderByPriceCateringAsc();
		List<CateringDto> cateringDtoList = cateringList.stream()
				.map(catering -> cateringAssembler.fromEntity(catering)).collect(Collectors.toList());
		return DefaultResponse.ok(cateringDtoList);
	}

	@GetMapping("/id")
	public DefaultResponse getId() {
		List<Object> catering = cateringRepository.getId();
		return DefaultResponse.ok(catering);
	}
	
	@GetMapping("/name")
	public DefaultResponse getName() {
		List<Object> catering = cateringRepository.getName();
		return DefaultResponse.ok(catering);
	}

	@GetMapping("/randomname")
	public DefaultResponse getRandomName(@RequestParam String nameCatering) {
		List<Catering> cateringList = cateringRepository.findAllByNameCateringIgnoreCaseContaining(nameCatering);
		List<CateringDto> cateringDtoList = cateringList.stream()
				.map(catering -> cateringAssembler.fromEntity(catering)).collect(Collectors.toList());
		return DefaultResponse.ok(cateringDtoList);
	}

	@GetMapping("/style/{styleCatering}")
	public DefaultResponse getbystyle(@PathVariable String styleCatering) {
		List<Catering> cateringList = cateringRepository.findAllByStyleCatering(styleCatering);
		List<CateringDto> dto = cateringList.stream().map(catering -> cateringAssembler.fromEntity(catering))
				.collect(Collectors.toList());
		return DefaultResponse.ok(dto);
	}
	
	@GetMapping("/betweenportion/{min}/{max}")
	public DefaultResponse getbetweenportion(@PathVariable Integer min, @PathVariable Integer max) {
		List<Catering> cateringList = cateringRepository.findAllBetweenPortion(min, max);
		List<CateringDto> dto = cateringList.stream().map(catering -> cateringAssembler.fromEntity(catering))
				.collect(Collectors.toList());
		return DefaultResponse.ok(dto);
	}

	@PostMapping
	public DefaultResponse insertCatering(@RequestBody CateringDto dto) {
		return DefaultResponse.ok(cateringService.insertCatering(dto));
	}

//	@PostMapping
//	public DefaultResponse insertplusImage(@RequestParam(required=false, name="model") String model,
//			@RequestParam(required=false, name="file") MultipartFile file) throws IllegalStateException, IOException {
//		cateringService.upload(file);
//		String url = "/images/" + file.getOriginalFilename();
//		ObjectMapper mapper = new ObjectMapper();
//		CateringDto dto = mapper.readValue(model, CateringDto.class);
//		return DefaultResponse.ok(cateringService.insertCatering(dto, url));
//	}

	@DeleteMapping("/{idCatering}")
	public DefaultResponse delete(@PathVariable Integer idCatering) {
		Catering catering = cateringRepository.findById(idCatering).get();
		String url = "/Users/lenovo/Documents/img-photo/" + catering.getImageCatering();
		File file = new File(url);
		file.delete();
		cateringRepository.deleteById(idCatering);
		return DefaultResponse.ok(catering);
	}

	@PutMapping("/edit/{idCatering}")
	public DefaultResponse update(@PathVariable Integer idCatering,
			@RequestParam(value = "model", required = false) String model,
			@RequestParam(value = "file", required = false) MultipartFile file)
			throws IllegalStateException, IOException {
		Catering catering = cateringRepository.findById(idCatering).get();
		ObjectMapper mapper = new ObjectMapper();
		if (model != null) {
			CateringDto dto = mapper.readValue(model, CateringDto.class);
			if (dto.getNameCatering() != null)
				catering.setNameCatering(dto.getNameCatering());
			if (dto.getPriceCatering() != null)
				catering.setPriceCatering(dto.getPriceCatering());
			if (dto.getDetailCatering() != null)
				catering.setDetailCatering(dto.getDetailCatering());
		}
		if (file != null) {
			String oldurl = catering.getImageCatering();
			File oldfile = new File(oldurl);
			oldfile.delete();
			cateringService.upload(file);
			String url = file.getOriginalFilename();
			catering.setImageCatering(url);
		}
		final Catering newcatering = cateringRepository.save(catering);
		return DefaultResponse.ok(newcatering);
	}

}
