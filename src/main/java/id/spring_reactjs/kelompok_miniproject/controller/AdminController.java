package id.spring_reactjs.kelompok_miniproject.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.spring_reactjs.kelompok_miniproject.assembler.AdminAssembler;
import id.spring_reactjs.kelompok_miniproject.configuration.DefaultResponse;
import id.spring_reactjs.kelompok_miniproject.model.dto.AdminDto;
import id.spring_reactjs.kelompok_miniproject.model.dto.MessageDto;
import id.spring_reactjs.kelompok_miniproject.model.dto.AdminDto;
import id.spring_reactjs.kelompok_miniproject.model.dto.AdminDto;
import id.spring_reactjs.kelompok_miniproject.model.dto.AdminDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Admin;
import id.spring_reactjs.kelompok_miniproject.model.entity.Admin;
import id.spring_reactjs.kelompok_miniproject.model.entity.Admin;
import id.spring_reactjs.kelompok_miniproject.model.entity.Admin;
import id.spring_reactjs.kelompok_miniproject.model.entity.Admin;
import id.spring_reactjs.kelompok_miniproject.repository.AdminRepository;
import id.spring_reactjs.kelompok_miniproject.service.AdminService;

@RestController
@RequestMapping(value = "/admin")
@CrossOrigin(origins = "http://localhost:3000")
public class AdminController {
	@Autowired
	private AdminRepository repository;
	@Autowired
	private AdminAssembler assembler;
	@Autowired
	private AdminService service;

	@PostMapping("/insertadmin")
	public DefaultResponse insertAdmin(@RequestBody AdminDto dto) {
		Admin admin = assembler.fromDto(dto);
		repository.save(admin);
		return DefaultResponse.ok(assembler.fromEntity(admin));
	}

//	@PostMapping("/insertadmin")
//	public DefaultResponse insertAdmin(@RequestParam("model") String model, @RequestParam("file") MultipartFile file)
//			throws IOException {
//		// Save as you want as per requirements
//		service.storeFile(file);
//		String url = file.getOriginalFilename();
//		ObjectMapper mapper = new ObjectMapper();
//		AdminDto dto = mapper.readValue(model, AdminDto.class);
//		dto.setImageAdmin(url);
//		dto = service.insertDataAdmin(dto);
//		return DefaultResponse.ok(dto);
//	}
//	
	@GetMapping("/all")
	public DefaultResponse getAllData() {
		List<Admin> adminList = repository.findAll();
		List<AdminDto> adminDtoList = adminList.stream().map(admin -> assembler.fromEntity(admin))
				.collect(Collectors.toList());
		return DefaultResponse.ok(adminDtoList);
	}

	@GetMapping("/loginadmin")
	public MessageDto getAdmin(@RequestParam String uname, @RequestParam String pass) {
		MessageDto messageDto = new MessageDto();
		if (repository.findByUnameAdmin(uname).isEmpty()) {
			messageDto.setStatus("fail");
			messageDto.setMessage("Account does not exist");
			return messageDto;
		} else {
			if (repository.findByUnameAdminLike(uname).getPassAdmin().equals(pass)) {
				messageDto.setStatus("success");
				messageDto.setMessage("Welcome " + uname + "!");
				Admin admin = repository.findByUnameAdminLike(uname);
				AdminDto adminDtoList = assembler.fromEntity(admin);
				messageDto.setDataAdmin(adminDtoList);
				return messageDto;
			} else {
				messageDto.setStatus("fail");
				messageDto.setMessage("Incorrect password");
				return messageDto;
			}
		}

	}

	@DeleteMapping("/delete/{id}")
	public DefaultResponse deleteById(@PathVariable Integer id) {
		Admin admin = repository.findByIdAdmin(id);
		String url = "/home/prosigmaka/Documents/img/venue/" + admin.getImageAdmin();
		File file = new File(url);
		file.delete();
		repository.deleteById(id);
		return DefaultResponse.ok(admin);
	}

}
