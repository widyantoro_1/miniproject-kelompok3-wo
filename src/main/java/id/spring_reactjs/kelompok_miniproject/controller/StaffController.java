package id.spring_reactjs.kelompok_miniproject.controller;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import id.spring_reactjs.kelompok_miniproject.assembler.StaffAssembler;
import id.spring_reactjs.kelompok_miniproject.configuration.DefaultResponse;
import id.spring_reactjs.kelompok_miniproject.model.dto.PhotoDto;
import id.spring_reactjs.kelompok_miniproject.model.dto.StaffDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Photo;
import id.spring_reactjs.kelompok_miniproject.model.entity.Staff;
import id.spring_reactjs.kelompok_miniproject.repository.StaffRepository;
import id.spring_reactjs.kelompok_miniproject.service.StaffService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/staff")
public class StaffController {
	@Autowired
	private StaffRepository staffRepository;
	@Autowired
	private StaffAssembler staffAssembler;
	@Autowired
	private StaffService staffService;

	@PostMapping("/data")
	public DefaultResponse insert(@RequestBody StaffDto dto) {
		return DefaultResponse.ok(staffService.insertStaff(dto));
	}

	@GetMapping("/alldata")
	public DefaultResponse getAll() {
		List<Staff> staffs = staffRepository.findAll();
		List<StaffDto> staffDtoList = staffs.stream().map(staff -> staffAssembler.fromEntity(staff))
				.collect(Collectors.toList());
		return DefaultResponse.ok(staffDtoList);
	}

	@GetMapping("/{idStaff}")
	public DefaultResponse getById(@PathVariable Integer idStaff) {
		StaffDto staffDto = staffAssembler.fromEntity(staffRepository.findById(idStaff).get());
		return DefaultResponse.ok(staffDto);
	}

	@DeleteMapping("/{idStaff}")
	public DefaultResponse deleteById(@PathVariable Integer idStaff) {
		StaffDto dto = staffAssembler.fromEntity(staffRepository.findById(idStaff).get());
		String url = "C:\\Users\\lenovo\\Documents\\img-photo\\" + dto.getProfile();
		File file = new File(url);
		file.delete();
		staffRepository.deleteById(idStaff);
		return DefaultResponse.ok(dto);
	}

	@PutMapping("/{idStaff}")
	public DefaultResponse updateById(@PathVariable Integer idStaff, @RequestBody StaffDto updateDto) {
		Staff entity = staffRepository.findById(idStaff).get();
		if (updateDto.getNameStaff() != null)
			entity.setNameStaff(updateDto.getNameStaff());
		if (updateDto.getJob() != null)
			entity.setJob(updateDto.getJob());
		if (updateDto.getProfile() != null)
			entity.setProfile(updateDto.getProfile());
		final Staff updateStaff = staffRepository.save(entity);
		return DefaultResponse.ok(updateStaff);
	}

	@GetMapping("/randomname/{name}")
	public DefaultResponse getRandomName(@PathVariable String name) {
		List<Staff> staffs = staffRepository.findAllByNameStaffIgnoreCaseContaining(name);
		List<StaffDto> staffDtoList = staffs.stream().map(staff -> staffAssembler.fromEntity(staff))
				.collect(Collectors.toList());
		return DefaultResponse.ok(staffDtoList);
	}

	@GetMapping("/by/{job}")
	public DefaultResponse findByJob(@PathVariable String job) {
		List<Staff> staffs = staffRepository.findAllByJob(job);
		List<StaffDto> staffDtoList = staffs.stream().map(staff -> staffAssembler.fromEntity(staff))
				.collect(Collectors.toList());
		return DefaultResponse.ok(staffDtoList);
	}

}
