package id.spring_reactjs.kelompok_miniproject.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.spring_reactjs.kelompok_miniproject.assembler.UserAssembler;
import id.spring_reactjs.kelompok_miniproject.configuration.DefaultResponse;
import id.spring_reactjs.kelompok_miniproject.model.dto.UserDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.User;
import id.spring_reactjs.kelompok_miniproject.repository.CateringRepository;
import id.spring_reactjs.kelompok_miniproject.repository.DamRepository;
import id.spring_reactjs.kelompok_miniproject.repository.DecorRepository;
import id.spring_reactjs.kelompok_miniproject.repository.PhotoRepository;
import id.spring_reactjs.kelompok_miniproject.repository.UserRepository;
import id.spring_reactjs.kelompok_miniproject.repository.VenueRepository;
import id.spring_reactjs.kelompok_miniproject.service.UserService;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {

	@Autowired
	private UserRepository repository;

	@Autowired
	private VenueRepository venueRepository;

	@Autowired
	private DecorRepository decorRepository;

	@Autowired
	private DamRepository damRepository;

	@Autowired
	private CateringRepository cateringRepository;

	@Autowired
	private PhotoRepository photoRepository;

	@Autowired
	private UserService service;

	@Autowired
	private UserAssembler booklistAssembler;

	@PostMapping("/bookedlist")
	public DefaultResponse insertBooklist(@RequestBody UserDto dto) {
		return DefaultResponse.ok(service.insertBookedlist(dto));
	}

	@PostMapping("/bookedlistEdit")
	public DefaultResponse editBooklist(@RequestBody UserDto dto) {
		Integer priceVenue = 0, priceDecor = 0, priceDam = 0, priceCatering = 0, pricePhoto = 0;
		User entity = repository.findById(dto.getIdUser()).get();
		if (dto.getIdUser() != null) {
			entity.setIdUser(dto.getIdUser());
		}
		if (dto.getNameBride() != null) {
			entity.setNameBride(dto.getNameBride());
		}
		if (dto.getNameGroom() != null) {
			entity.setNameGroom(dto.getNameGroom());
		}
		if (dto.getEmail() != null) {
			entity.setEmail(dto.getEmail());
		}
		if (dto.getNoHp() != null) {
			entity.setNoHp(dto.getNoHp());
		}
		if (dto.getWeddingDate() != null) {
			entity.setWeddingDate(dto.getWeddingDate());
		}
		if (dto.getGuestNumber() != null) {
			entity.setGuestNumber(dto.getGuestNumber());
		}
		if (dto.getNameVenue() != null) {
			entity.setIdVenue(venueRepository.findByNameVenue(dto.getNameVenue()).getIdVenue());
			priceVenue = venueRepository.findByIdVenue(entity.getIdVenue()).getPriceVenue();
		}
		if (dto.getNameDecor() != null) {
			entity.setIdDecor(decorRepository.findByNameDecor(dto.getNameDecor()).getIdDecor());
			priceDecor = decorRepository.findAllByIdDecor(entity.getIdDecor()).getPriceDecor();
		}
		if (dto.getNameDam() != null) {
			entity.setIdDam(damRepository.findByNameDam(dto.getNameDam()).getIdDam());
			priceDam = damRepository.findByIdDam(entity.getIdDam()).getPriceDam();
		}
		if (dto.getNameCatering() != null) {
			entity.setIdCatering(cateringRepository.getByName(dto.getNameCatering()).getIdCatering());
			priceCatering = cateringRepository.findAllByIdCatering(entity.getIdCatering()).getPriceCatering();
		}
		if (dto.getNamePhoto() != null) {
			entity.setIdPhoto(photoRepository.findByNamePhoto(dto.getNamePhoto()).getIdPhoto());
			pricePhoto = photoRepository.findByIdPhoto(entity.getIdPhoto()).getPricePhoto();
		}

		Integer totalPrice = priceVenue + priceDecor + priceDam + priceCatering + pricePhoto;
		if (dto.getTotalPrice() != null) {
			entity.setTotalPrice(totalPrice);
		}
		if(dto.getReference() !=null) {
			entity.setReference(dto.getReference());
		}
		if(dto.getProgress() != null) {
			entity.setProgress(dto.getProgress());
		}
		if(dto.getUpdatedDate() != null) {
			entity.setUpdatedDate(dto.getUpdatedDate());
		}
		if(dto.getAdmin() !=null) {
			entity.setAdmin(dto.getAdmin());
		}

		return DefaultResponse.ok(service.editBookedlist(entity));
	}

//  http://localhost:1212/v1/app/user
	@GetMapping
	public DefaultResponse getBooklist() {
		List<User> userList = repository.findAll();
		List<UserDto> booklist = userList.stream().map(user -> booklistAssembler.fromEntity(user))
				.collect(Collectors.toList());
		return DefaultResponse.ok(booklist);
	}
	
	@GetMapping("/{idUser}")
	public DefaultResponse getUserbyid(@PathVariable Integer idUser) {
		User user = repository.findByIdUser(idUser);
		UserDto dto = booklistAssembler.fromEntity(user);
		ArrayList<Object> users = new ArrayList<Object>();
		users.add(dto);
		return DefaultResponse.ok(users);
	}

//  http://localhost:1212/v1/app/user/date
	@GetMapping("/date")
	public DefaultResponse getDate() {
		List<Object> user = repository.getDate();
		return DefaultResponse.ok(user);
	}

//  http://localhost:1212/v1/app/user/{idUser}
	@DeleteMapping("/{idUser}")
	public DefaultResponse deleteBooklist(@PathVariable Integer idUser) {
		User user = repository.findByIdUser(idUser);
		repository.deleteById(idUser);
		return DefaultResponse.ok(user);
	}

//  http://localhost:1212/v1/app/user/{idUser}
	@PutMapping("/{idUser}")
	public DefaultResponse updateBooklist(@PathVariable Integer idUser, @RequestBody User newUser) {
		User oldUser = repository.findById(idUser).get();
		oldUser.setNameBride(newUser.getNameBride());
		oldUser.setNameGroom(newUser.getNameGroom());
		oldUser.setEmail(newUser.getEmail());
		oldUser.setNoHp(newUser.getNoHp());
		oldUser.setWeddingDate(newUser.getWeddingDate());
		oldUser.setGuestNumber(newUser.getGuestNumber());
		oldUser.setIdVenue(newUser.getIdVenue());
		oldUser.setIdDecor(newUser.getIdDecor());
		oldUser.setIdDam(newUser.getIdDam());
		oldUser.setIdCatering(newUser.getIdCatering());
		oldUser.setIdPhoto(newUser.getIdPhoto());
//		oldUser.setVenue(venueRepository.findByNameVenue(newUser.getVenue().getNameVenue()));
//		oldUser.setDecor(decorRepository.findByNameDecor(newUser.getDecor().getNameDecor()));
//		oldUser.setDam(damRepository.findByNameDam(newUser.getDam().getNameDam()));
//		oldUser.setCatering(cateringRepository.getByName(newUser.getCatering().getNameCatering()));
//		oldUser.setPhoto(photoRepository.findByNamePhoto(newUser.getPhoto().getNamePhoto()));
//		oldUser.setVenue(newUser.getVenue());
//		oldUser.setDecor(newUser.getDecor());	
//		oldUser.setDam(newUser.getDam());
//		oldUser.setCatering(newUser.getCatering());
//		oldUser.setPhoto(newUser.getPhoto());
		oldUser.setTotalPrice(newUser.getTotalPrice());
		final User editBooklist = repository.save(oldUser);
		return DefaultResponse.ok(editBooklist);
	}
}
