package id.spring_reactjs.kelompok_miniproject.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.spring_reactjs.kelompok_miniproject.assembler.DamAssembler;
import id.spring_reactjs.kelompok_miniproject.configuration.DefaultResponse;
import id.spring_reactjs.kelompok_miniproject.model.dto.CateringDto;
import id.spring_reactjs.kelompok_miniproject.model.dto.DamDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Catering;
import id.spring_reactjs.kelompok_miniproject.model.entity.Dam;
import id.spring_reactjs.kelompok_miniproject.repository.DamRepository;
import id.spring_reactjs.kelompok_miniproject.service.DamService;

@RestController
@RequestMapping("/dam")
@CrossOrigin(origins = "http://localhost:3000")
public class DamController {
	@Autowired
	private DamRepository repository;

	@Autowired
	private DamService service;

	@Autowired
	private DamAssembler assembler;

//  http://localhost:1212/v1/app/dam
	@PostMapping
	public DefaultResponse insertDam(@RequestBody DamDto dto) {
		return DefaultResponse.ok(service.insertDam(dto));
	}

//  http://localhost:1212/v1/app/dam
	@GetMapping
	public DefaultResponse getDam() {
		List<Dam> damList = repository.findAll();
		List<DamDto> damDtoList = damList.stream().map(dam -> assembler.fromEntity(dam)).collect(Collectors.toList());
		return DefaultResponse.ok(damDtoList);
	}

//  http://localhost:1212/v1/app/dam/{idDam}
	@GetMapping("/{idDam}")
	public DefaultResponse getDamById(@PathVariable Integer idDam) {
		DamDto damDto = assembler.fromEntity(repository.findById(idDam).get());
		return DefaultResponse.ok(damDto);
	}

//  http://localhost:1212/v1/app/dam/id
	@GetMapping("/id")
	public DefaultResponse getId() {
		List<Object> dam = repository.getId();
		return DefaultResponse.ok(dam);
	}

	@GetMapping("/name")
	public DefaultResponse getName() {
		List<Object> dam = repository.getName();
		return DefaultResponse.ok(dam);
	}

//  http://localhost:1212/v1/app/dam/style/{styleDam}
	@GetMapping("/style/{styleDam}")
	public DefaultResponse getDamByStyle(@PathVariable String styleDam) {
		List<Dam> damList = repository.findAllByStyleDam(styleDam);
		List<DamDto> damDtoList = new ArrayList<>();
		for (Dam dam : damList) {
			DamDto damDto = assembler.fromEntity(dam);
			damDtoList.add(damDto);
		}
		return DefaultResponse.ok(damDtoList);
	}

	@GetMapping("/randomname")
	public DefaultResponse getRandomName(@RequestParam String nameDam) {
		List<Dam> damList = repository.findAllByNameDamIgnoreCaseContaining(nameDam);
		List<DamDto> damDtoList = damList.stream().map(dam -> assembler.fromEntity(dam)).collect(Collectors.toList());
		return DefaultResponse.ok(damDtoList);
	}

//	http://localhost:1212/v1/app/dam/{idDam}
	@DeleteMapping("/{idDam}")
	public DefaultResponse deleteDam(@PathVariable Integer idDam) {
		Dam dam = repository.findByIdDam(idDam);
		String url = dam.getImageDam();
		File file = new File(url);
		file.delete();
		repository.deleteById(idDam);
		return DefaultResponse.ok(dam);
	}

}

//http://localhost:1212/v1/app/dam
//	@PostMapping("/upload")
//	public DefaultResponse uploadDam(@RequestParam("info") String info, @RequestParam("file") MultipartFile file)
//			throws IllegalStateException, IOException {
//		service.uploadDam(file);
//		String url = file.getOriginalFilename();
//		ObjectMapper mapper = new ObjectMapper();
//		DamDto dto = mapper.readValue(info, DamDto.class);
//		dto.setImageDam(url);
//		return DefaultResponse.ok(service.insertDam(dto, url));
//	}

//http://localhost:1212/v1/app/dam/asc/{styleDam}
//@GetMapping("/asc/{styleDam}")
//public DefaultResponse styleSortDamAsc(@RequestParam String styleDam) {
//	List<Dam> damList = repository.findAllByStyleDamOrderByPriceDamAsc(styleDam);
//	List<DamDto> damDtoList = new ArrayList<>();
//	for (Dam dam : damList) {
//		DamDto damDto = assembler.fromEntity(dam);
//		damDtoList.add(damDto);
//	}
//	return DefaultResponse.ok(damDtoList);
//}

//http://localhost:1212/v1/app/dam/desc/{styleDam}
//@GetMapping("/desc/{styleDam}")
//public DefaultResponse styleSortDamDesc(@RequestParam String styleDam) {
//	List<Dam> damList = repository.findAllByStyleDamOrderByPriceDamDesc(styleDam);
//	List<DamDto> damDtoList = new ArrayList<>();
//	for (Dam dam : damList) {
//		DamDto damDto = assembler.fromEntity(dam);
//		damDtoList.add(damDto);
//	}
//	return DefaultResponse.ok(damDtoList);
//}

//http://localhost:1212/v1/app/dam/asc
//@GetMapping("/asc")
//public DefaultResponse sortDamAsc() {
//	List<Dam> damList = repository.findAllByOrderByPriceDamAsc();
//	List<DamDto> damDtoList = new ArrayList<>();
//	for (Dam dam : damList) {
//		DamDto damDto = assembler.fromEntity(dam);
//		damDtoList.add(damDto);
//	}
//	return DefaultResponse.ok(damDtoList);
//}

//http://localhost:1212/v1/app/dam/desc
//@GetMapping("/desc")
//public DefaultResponse sortDamDesc() {
//	List<Dam> damList = repository.findAllByOrderByPriceDamDesc();
//	List<DamDto> damDtoList = new ArrayList<>();
//	for (Dam dam : damList) {
//		DamDto damDto = assembler.fromEntity(dam);
//		damDtoList.add(damDto);
//	}
//	return DefaultResponse.ok(damDtoList);
//}

//http://localhost:1212/v1/app/dam/{idDam}
//@PutMapping("/{idDam}")
//public DefaultResponse updateDam(@PathVariable Integer idDam,
//		@RequestParam(value = "info", required = false) String info,
//		@RequestParam(value = "file", required = false) MultipartFile file)
//		throws IllegalStateException, IOException {
//	Dam dam = repository.findById(idDam).get();
//	ObjectMapper mapper = new ObjectMapper();
//	if (info != null) {
//		DamDto dto = mapper.readValue(info, DamDto.class);
//		if (dto.getNameDam() != null)
//			dam.setNameDam(dto.getNameDam());
//		if (dto.getStyleDam() != null)
//			dam.setStyleDam(dto.getStyleDam());
//		if (dto.getPriceDam() != null)
//			dam.setPriceDam(dto.getPriceDam());
//		if (dto.getDetailDam() != null)
//			dam.setDetailDam(dto.getDetailDam());
//	}
//
//	if (file != null) {
//		String oldUrl = dam.getImageDam();
//		File oldFile = new File(oldUrl);
//		oldFile.delete();
//		service.uploadDam(file);
//		String url = file.getOriginalFilename();
//		dam.setImageDam(url);
//	}
//
//	final Dam editDam = repository.save(dam);
//	return DefaultResponse.ok(editDam);
//}