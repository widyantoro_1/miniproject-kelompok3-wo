package id.spring_reactjs.kelompok_miniproject.controller;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.spring_reactjs.kelompok_miniproject.assembler.PhotoAssembler;
import id.spring_reactjs.kelompok_miniproject.configuration.DefaultResponse;
import id.spring_reactjs.kelompok_miniproject.model.dto.PhotoDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Photo;
import id.spring_reactjs.kelompok_miniproject.repository.PhotoRepository;
import id.spring_reactjs.kelompok_miniproject.service.PhotoService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/photo&video")
public class PhotoController {
	@Autowired
	private PhotoRepository photoRepository;
	@Autowired
	private PhotoAssembler photoAssembler;
	@Autowired
	private PhotoService photoService;

	@PostMapping("/data")
	public DefaultResponse insert(@RequestBody PhotoDto dto) {
		return DefaultResponse.ok(photoService.insertPhotoVideo(dto));
	}

	@GetMapping("/alldata")
	public DefaultResponse getAll() {
		List<Photo> photos = photoRepository.findAll();
		List<PhotoDto> photoDtoList = photos.stream().map(photo ->
		photoAssembler.fromEntity(photo)).collect(Collectors.toList());
		return DefaultResponse.ok(photoDtoList);
	}

	@GetMapping("/{idPhoto}")
	public DefaultResponse getById(@PathVariable Integer idPhoto) {
		PhotoDto photoDto = photoAssembler.fromEntity(photoRepository.findById(idPhoto).get());
		return DefaultResponse.ok(photoDto);
	}

	@GetMapping("/theme/{theme}")
	public DefaultResponse getByTheme(@PathVariable String theme) {
		List<Photo> photos = photoRepository.findAllByThemePhoto(theme);
		List<PhotoDto> photoDtoList = photos.stream().map(photo -> photoAssembler.fromEntity(photo))
				.collect(Collectors.toList());
		return DefaultResponse.ok(photoDtoList);
	}
	
	@GetMapping("/name")
	public DefaultResponse getName() {
		List<Object> photo = photoRepository.getName();
		return DefaultResponse.ok(photo);
	}

	@DeleteMapping("/{idPhoto}")
	public DefaultResponse deleteById(@PathVariable Integer idPhoto) {
		Photo photo = photoRepository.findById(idPhoto).get();
		String url = "C:\\Users\\lenovo\\Documents\\img-photo\\"+ photo.getImagePhoto();
		File file = new File(url);
		file.delete();
		photoRepository.deleteById(idPhoto);
		return DefaultResponse.ok(photo);
	}

	
	@GetMapping("/randomname")
	public DefaultResponse getRandomName(@RequestParam String namePhoto) {
		List<Photo> photoList = photoRepository.findAllByNamePhotoIgnoreCaseContaining(namePhoto);
		List<PhotoDto> photoDtoList = photoList.stream()
				.map(photo -> photoAssembler.fromEntity(photo)).collect(Collectors.toList());
		return DefaultResponse.ok(photoDtoList);
	}
	
//	@GetMapping("/id")
//	public DefaultResponse getId() {
//		List<Object> photo = photoRepository.getId();
//		return DefaultResponse.ok(photo);
//	}

//	@GetMapping("/name/{namePhoto}")
//	public DefaultResponse getByName(@PathVariable String namePhoto) {
//		PhotoDto dto = photoAssembler.fromEntity(photoRepository.findByNamePhoto(namePhoto));
//		return DefaultResponse.ok(dto);
//	}
	
//	@PostMapping("/dataupload")
//	public DefaultResponse insertUpload(@RequestParam("model") String model, @RequestParam("file") MultipartFile file)
//			throws IllegalStateException, IOException {
//		photoService.upload(file);
//		String url = file.getOriginalFilename();
////		PhotoDto dto = new PhotoDto();
////		dto.setNamePhoto(namePhoto);
////		dto.setPricePhoto(pricePhoto);
////		dto.setDetailPhoto(detailPhoto);
//		ObjectMapper mapper = new ObjectMapper();
//		PhotoDto dto = mapper.readValue(model, PhotoDto.class);
//		dto.setImagePhoto(url);
//		return DefaultResponse.ok(photoService.insertUpload(dto));
//	}
	
//	@GetMapping("/priceAsc")
//	public DefaultResponse getByPriceAsc() {
//		List<Photo> photos = photoRepository.findAllByOrderByPricePhotoAsc();
//		List<PhotoDto> photoDtoList = photos.stream().map(photo -> photoAssembler.fromEntity(photo))
//				.collect(Collectors.toList());
//		return DefaultResponse.ok(photoDtoList);
//	}
//	
//	@GetMapping("/priceDesc")
//	public DefaultResponse getByPriceDesc() {
//		List<Photo> photos = photoRepository.findAllByOrderByPricePhotoDesc();
//		List<PhotoDto> photoDtoList = photos.stream().map(photo -> photoAssembler.fromEntity(photo))
//				.collect(Collectors.toList());
//		return DefaultResponse.ok(photoDtoList);
//	}
//	
//	@GetMapping("/theme/Asc")
//	public DefaultResponse getByThemeAsc(@RequestParam String theme) {
//		List<Photo> photos = photoRepository.findAllByThemePhotoOrderByPricePhotoAsc(theme);
//		List<PhotoDto> photoDtoList = photos.stream().map(photo -> photoAssembler.fromEntity(photo))
//				.collect(Collectors.toList());
//		return DefaultResponse.ok(photoDtoList);
//	}
//	
//	@GetMapping("/theme/Desc")
//	public DefaultResponse getByThemeDesc(@RequestParam String theme) {
//		List<Photo> photos = photoRepository.findAllByThemePhotoOrderByPricePhotoDesc(theme);
//		List<PhotoDto> photoDtoList = photos.stream().map(photo -> photoAssembler.fromEntity(photo))
//				.collect(Collectors.toList());
//		return DefaultResponse.ok(photoDtoList);
//	}

//	@PutMapping("/{idPhoto}")
//	public DefaultResponse updateById(@PathVariable Integer idPhoto,
//			@RequestParam(value = "model", required = false) String model,
//			@RequestParam(value = "file", required = false) MultipartFile file)
//			throws IllegalStateException, IOException {
//		Photo entity = photoRepository.findById(idPhoto).get();
//		ObjectMapper mapper = new ObjectMapper();
//		if (model != null) {
//			PhotoDto dto = mapper.readValue(model, PhotoDto.class);
//			if (dto.getNamePhoto() != null)
//				entity.setNamePhoto(dto.getNamePhoto());
//			if (dto.getThemePhoto() != null)
//				entity.setThemePhoto(dto.getThemePhoto());
//			if (dto.getDetailPhoto() != null)
//				entity.setDetailPhoto(dto.getDetailPhoto());
//			if (dto.getPricePhoto() != null)
//				entity.setPricePhoto(dto.getPricePhoto());
//		}
//		if (file != null) {
//			String oldUrl = entity.getImagePhoto();
//			File oldFile = new File(oldUrl);
//			oldFile.delete();
//			photoService.upload(file);
//			String url = file.getOriginalFilename();
//			entity.setImagePhoto(url);
//		}
//
//		final Photo updatePhoto = photoRepository.save(entity);
//		return DefaultResponse.ok(updatePhoto);
//	}
}
