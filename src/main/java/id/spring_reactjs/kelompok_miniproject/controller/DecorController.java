package id.spring_reactjs.kelompok_miniproject.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.spring_reactjs.kelompok_miniproject.assembler.DecorAssembler;
import id.spring_reactjs.kelompok_miniproject.configuration.DefaultResponse;
import id.spring_reactjs.kelompok_miniproject.model.dto.CateringDto;
import id.spring_reactjs.kelompok_miniproject.model.dto.DecorDto;
import id.spring_reactjs.kelompok_miniproject.model.dto.PhotoDto;
import id.spring_reactjs.kelompok_miniproject.model.dto.DecorDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Catering;
import id.spring_reactjs.kelompok_miniproject.model.entity.Decor;
import id.spring_reactjs.kelompok_miniproject.model.entity.Photo;
import id.spring_reactjs.kelompok_miniproject.model.entity.Decor;
import id.spring_reactjs.kelompok_miniproject.repository.DecorRepository;
import id.spring_reactjs.kelompok_miniproject.service.DecorService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/decor")
public class DecorController {
	@Autowired
	private DecorRepository repository;
	@Autowired
	private DecorService service;
	@Autowired
	private DecorAssembler assembler;
	
//	http://localhost:1212/v1/app/decor
	@PostMapping("/inputdata")
	public DefaultResponse insert(@RequestBody DecorDto dto) {
		return DefaultResponse.ok(service.insertDecor(dto));
	}
//	@PostMapping("/inputplusimage")
//	public DefaultResponse insertplusImage(@RequestParam("model") String model, @RequestParam("file") MultipartFile file) throws IllegalStateException, IOException {
//		service.uploadFile(file);
//		String url = file.getOriginalFilename();
//		ObjectMapper mapper = new ObjectMapper();
//		DecorDto dto = mapper.readValue(model, DecorDto.class);
//		return DefaultResponse.ok(service.insertDecor(dto, url));
//	}
//	
	
//	http://localhost:1212/v1/app/decor
	@GetMapping("/all")
	public DefaultResponse getDecor() {
		List<Decor> decorList = repository.findAll();
		List<DecorDto> decorDtoList = decorList.stream().map(decor -> assembler.fromEntity(decor))
				.collect(Collectors.toList());
		return DefaultResponse.ok(decorDtoList);
	}

//	http://localhost:1212/v1/app/dcor/{idDecor}
	@GetMapping("/{idDecor}")
	public DefaultResponse getDecorById(@PathVariable Integer idDecor) {
		DecorDto decorDto = assembler.fromEntity(repository.findById(idDecor).get());
		return DefaultResponse.ok(decorDto);
	}
	
//	http://localhost:1212/v1/app/decor/{styleDecor}
	@GetMapping("/style/{styleDecor}")
	public DefaultResponse getDecorByStyle(@PathVariable String styleDecor) {
		List<Decor> decorList = repository.findByStyleDecor(styleDecor);
		List<DecorDto> decorDtoList = new ArrayList<>();
		for (Decor decor : decorList) {
			DecorDto decorDto = assembler.fromEntity(decor);
			decorDtoList.add(decorDto);
		}
		return DefaultResponse.ok(decorDtoList);
	}
	
	@GetMapping("/id")
	public DefaultResponse getId() {
		List<Object> decor = repository.getId();
		return DefaultResponse.ok(decor);
	}
	
	@GetMapping("/name")
	public DefaultResponse getName() {
		List<Object> decor = repository.getName();
		return DefaultResponse.ok(decor);
	}
	
//	http://localhost:1212/v1/app/decor/{idDecor}
	@DeleteMapping("/delete/{idDecor}")
	public DefaultResponse deleteById(@PathVariable Integer idDecor) {
		Decor decor = repository.findById(idDecor).get();
		String url = "/Users/lenovo/Documents/img-photo/"+ decor.getImageDecor();
		File file = new File(url);
		file.delete();
		repository.deleteById(idDecor);
		return DefaultResponse.ok(decor);
	}
	
//	http://localhost:1212/v1/app/decor/{idDecor}
	@PutMapping("/edit/{idCatering}")
	public DefaultResponse update(@PathVariable Integer idDecor,
			@RequestParam(value = "model", required = false) String model,
			@RequestParam(value = "file", required = false) MultipartFile file)
			throws IllegalStateException, IOException {
		Decor decor = repository.findById(idDecor).get();
		ObjectMapper mapper = new ObjectMapper();
		if (model != null) {
			DecorDto dto = mapper.readValue(model, DecorDto.class);
			if (dto.getNameDecor() != null)
				decor.setNameDecor(dto.getNameDecor());
			if (dto.getPriceDecor() != null)
				decor.setPriceDecor(dto.getPriceDecor());
			if (dto.getDetailDecor() != null)
				decor.setDetailDecor(dto.getDetailDecor());
		}
		if (file != null) {
			String oldurl = decor.getImageDecor();
			File oldfile = new File(oldurl);
			oldfile.delete();
			service.uploadFile(file);
			String url =file.getOriginalFilename();
			decor.setImageDecor(url);
		}
		final Decor decoredit = repository.save(decor);
		return DefaultResponse.ok(decoredit);
	}
	
	@GetMapping("/randomname")
	public DefaultResponse getRandomName(@RequestParam String nameDecor) {
		List<Decor> decorList = repository.findAllByNameDecorIgnoreCaseContaining(nameDecor);
		List<DecorDto> decorDtoList = decorList.stream().map(decor -> assembler.fromEntity(decor))
				.collect(Collectors.toList());
		return DefaultResponse.ok(decorDtoList);
	}
	
}