package id.spring_reactjs.kelompok_miniproject.controller;

import id.spring_reactjs.kelompok_miniproject.assembler.AssignmentAssembler;
import id.spring_reactjs.kelompok_miniproject.assembler.UserAssembler;
import id.spring_reactjs.kelompok_miniproject.assembler.StaffAssembler;
import id.spring_reactjs.kelompok_miniproject.model.dto.AssignmentDto;
import id.spring_reactjs.kelompok_miniproject.model.dto.UserDto;
import id.spring_reactjs.kelompok_miniproject.model.dto.StaffDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Assignment;
import id.spring_reactjs.kelompok_miniproject.model.entity.User;
import id.spring_reactjs.kelompok_miniproject.model.entity.Staff;
import id.spring_reactjs.kelompok_miniproject.repository.AssignmentRepository;
import id.spring_reactjs.kelompok_miniproject.repository.UserRepository;
import id.spring_reactjs.kelompok_miniproject.repository.StaffRepository;
import id.spring_reactjs.kelompok_miniproject.service.AssignmentService;
import id.spring_reactjs.kelompok_miniproject.configuration.DefaultResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/assignment")
@CrossOrigin(origins = "http://localhost:3000")
public class AssignmentController {

	@Autowired
	private AssignmentRepository assignmentRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private StaffRepository staffRepository;

	@Autowired
	private AssignmentService assignmentService;

	@Autowired
	private AssignmentAssembler assignmentAssembler;

	@Autowired
	private UserAssembler bookedAssembler;

	@Autowired
	private StaffAssembler staffAssembler;

	@PostMapping
	public DefaultResponse insert(@RequestBody AssignmentDto dto) {
		return DefaultResponse.ok(assignmentService.insertAssignment(dto));
	}

	@GetMapping
	public DefaultResponse get() {
		List<Assignment> assignmentList = assignmentRepository.findAll();
		List<AssignmentDto> assignmentDtoList = assignmentList.stream()
				.map(assignment -> assignmentAssembler.fromEntity(assignment)).collect(Collectors.toList());
		return DefaultResponse.ok(assignmentDtoList);
	}

	@GetMapping("/newuser")
	public DefaultResponse getNew() {
		List<User> userList = userRepository.findNewUser();
		List<UserDto> dto = userList.stream().map(user -> bookedAssembler.fromEntity(user))
				.collect(Collectors.toList());
		return DefaultResponse.ok(dto);
	}

	@GetMapping("/restuser")
	public DefaultResponse getUser() {
		List<User> users = userRepository.findRestUser();
		List<UserDto> dto = users.stream().map(u -> bookedAssembler.fromEntity(u)).collect(Collectors.toList());
		return DefaultResponse.ok(dto);
	}

	@GetMapping("/reststaff")
	public DefaultResponse getStaff() {
		List<Staff> staffs = staffRepository.findLastStaffSchedule();
		List<StaffDto> dto = staffs.stream().map(s -> staffAssembler.fromEntity(s)).collect(Collectors.toList());
		return DefaultResponse.ok(dto);
	}
	
	@GetMapping("/reststaff/byjob/{job}")
	public DefaultResponse getStaffByJob(@PathVariable String job) {
		List<Staff> staffs = staffRepository.findLastStaffScheduleByJob(job);
		List<StaffDto> dto = staffs.stream().map(s -> staffAssembler.fromEntity(s)).collect(Collectors.toList());
		return DefaultResponse.ok(dto);
	}
	
	@GetMapping("/userbyid/{idUser}")
	public DefaultResponse getUserbyid(@PathVariable Integer idUser) {
		User user = userRepository.findByIdUser(idUser);
		UserDto dto = bookedAssembler.fromEntity(user);
		ArrayList<Object> users = new ArrayList<Object>();
		users.add(dto);
		return DefaultResponse.ok(users);
	}
	
	@GetMapping("/assignmentbyuserid/{idUser}")
	public DefaultResponse getAssigByUserId(@PathVariable Integer idUser) {
		List<Assignment> assignments = assignmentRepository.findAllByUserIdUser(idUser);
		List<AssignmentDto> dto = assignments.stream().map(a -> assignmentAssembler.fromEntity(a))
				.collect(Collectors.toList());
		return DefaultResponse.ok(dto);
	}
	
	@GetMapping("/assignmentbystaffid/{idStaff}")
	public DefaultResponse getAssignByStaffId(@PathVariable Integer idStaff) {
		List<Assignment> assignments = assignmentRepository.findAllByStaffIdStaff(idStaff);
		List<AssignmentDto> dto = assignments.stream().map(a -> assignmentAssembler.fromEntity(a))
				.collect(Collectors.toList());
		return DefaultResponse.ok(dto);
	}

	@DeleteMapping("{idAssignment}")
	public DefaultResponse delete(@PathVariable Integer idAssignment) {
		Assignment assignment = assignmentRepository.findById(idAssignment).get();
		User user = userRepository.findByIdUser(assignment.getUser().getIdUser());
		user.setCountProduct(user.getCountProduct() + 1);
		assignmentRepository.deleteById(idAssignment);
		return DefaultResponse.ok(assignment);
	}

}
