package id.spring_reactjs.kelompok_miniproject.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.spring_reactjs.kelompok_miniproject.assembler.VenueAssembler;
import id.spring_reactjs.kelompok_miniproject.configuration.DefaultResponse;
import id.spring_reactjs.kelompok_miniproject.model.dto.VenueDto;
import id.spring_reactjs.kelompok_miniproject.model.entity.Venue;
import id.spring_reactjs.kelompok_miniproject.repository.VenueRepository;
import id.spring_reactjs.kelompok_miniproject.service.VenueService;

@RestController
@RequestMapping("/venue")
@CrossOrigin(origins = "http://localhost:3000")
public class VenueController {
	@Autowired
	private VenueRepository repository;
	@Autowired
	private VenueService service;
	@Autowired
	private VenueAssembler assembler;

	// http://localhost:1212/venue

	/* Insert Data */
	@PostMapping("/data")
	public DefaultResponse insertVenue(@RequestBody VenueDto dto) {
		return DefaultResponse.ok(service.insertDataVenue(dto));
	}

	@GetMapping("/location")
	public DefaultResponse getLocation(@RequestParam String loc) {
		List<Venue> venueList = repository.findAllByLocation(loc);
		List<VenueDto> venueDtoList = venueList.stream().map(venue -> assembler.fromEntity(venue))
				.collect(Collectors.toList());
		return DefaultResponse.ok(venueDtoList);

	}

	@GetMapping("/ambiance")
	public DefaultResponse getAmbiance(@RequestParam String amb) {
		List<Venue> venueList = repository.findAllByAmbiance(amb);
		List<VenueDto> venueDtoList = venueList.stream().map(venue -> assembler.fromEntity(venue))
				.collect(Collectors.toList());
		return DefaultResponse.ok(venueDtoList);

	}

	@GetMapping("/locamb")
	public DefaultResponse getLocAmb(@RequestParam String loc, @RequestParam String amb) {
		List<Venue> venueList = repository.findAllByLocationAndAmbiance(loc, amb);
		List<VenueDto> venueDtoList = venueList.stream().map(venue -> assembler.fromEntity(venue))
				.collect(Collectors.toList());
		return DefaultResponse.ok(venueDtoList);

	}

	@GetMapping("/all")
	public DefaultResponse getAllData() {
		List<Venue> venueList = repository.findAll();
		List<VenueDto> venueDtoList = venueList.stream().map(venue -> assembler.fromEntity(venue))
				.collect(Collectors.toList());
		return DefaultResponse.ok(venueDtoList);
	}

	@GetMapping("/id")
	public DefaultResponse getById() {
		List<Object> venue = repository.getId();
		return DefaultResponse.ok(venue);
	}
	
	@GetMapping("/name")
	public DefaultResponse getByName() {
		List<Object> venue = repository.getName();
		return DefaultResponse.ok(venue);
	}

	@PostMapping
	public DefaultResponse insert(@RequestBody VenueDto dto) {
		Venue venue = assembler.fromDto(dto);
		repository.save(venue);
		return DefaultResponse.ok(assembler.fromEntity(venue));
	}

	@DeleteMapping("/delete/{id}")
	public DefaultResponse deleteById(@PathVariable Integer id) {
		Venue venue = repository.findByIdVenue(id);

//		String url = "/home/prosigmaka/Documents/reactjs_project/frontend-react-weddingorganizer/react-wedding-organizer/kerinci-stories/src/img/ImgVenue" + venue.getImageVenue();
		String url = "/home/prosigmaka/Documents/img/venue" + venue.getImageVenue();
		File file = new File(url);
		file.delete();

		repository.deleteById(id);
		return DefaultResponse.ok(venue);
	}

	@PutMapping("/edit/{id}")
	public DefaultResponse updateById(@PathVariable Integer id,
			@RequestParam(value = "model", required = false) String model,
			@RequestParam(value = "file", required = false) MultipartFile file)
			throws IllegalStateException, IOException {
		Venue venue = repository.findByIdVenue(id);
		ObjectMapper mapper = new ObjectMapper();
		if (model != null) {
			VenueDto dto = mapper.readValue(model, VenueDto.class);
			if (dto.getNameVenue() != null)
				venue.setNameVenue(dto.getNameVenue());
			if (dto.getLocation() != null)
				venue.setLocation(dto.getLocation());
			if (dto.getAmbiance() != null)
				venue.setAmbiance(dto.getAmbiance());
			if (dto.getCapacity() != null)
				venue.setCapacity(dto.getCapacity());
			if (dto.getDetailVenue() != null)
				venue.setDetailVenue(dto.getDetailVenue());
			if (dto.getPriceVenue() != null)
				venue.setPriceVenue(dto.getPriceVenue());
		}
		if (file != null) {
			String urlOld = venue.getImageVenue();
			File fileOld = new File(urlOld);
			fileOld.delete();
			service.storeFile(file);
			String url = file.getOriginalFilename();
			venue.setImageVenue(url);
		}

		final Venue updateVenue = repository.save(venue);

		return DefaultResponse.ok(updateVenue);
	}
	
	@GetMapping("/randomname")
	public DefaultResponse getRandomName(@RequestParam String nameVenue) {
		List<Venue> venueList = repository.findAllByNameVenueIgnoreCaseContaining(nameVenue);
		List<VenueDto> venueDtoList = venueList.stream().map(venue -> assembler.fromEntity(venue))
				.collect(Collectors.toList());
		return DefaultResponse.ok(venueDtoList);
	}

}
