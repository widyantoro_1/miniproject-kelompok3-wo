package id.spring_reactjs.kelompok_miniproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.spring_reactjs.kelompok_miniproject.model.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	User findByIdUser(Integer idUser);
	
	User findByVenueNameVenue(String nameVenue);
	
	User findByEmailLike(String email);
	
	List<User> findByEmail(String email);

	@Query(value = "SELECT wedding_date FROM t_user", nativeQuery = true)
	List<Object> getDate();

	@Query(value = "SELECT u.id_user, u.name_bride, u.name_groom, u.email, u.no_hp, u.wedding_date, "
			+ "u.guest_number, u.id_venue, u.id_decor, u.id_dam, u.id_catering, u.id_photo, u.total_price "
			+ "FROM t_user u LEFT JOIN assignment a ON u.id_user = a.id_user WHERE a.id_user ISNULL", nativeQuery = true)
	List<User> findNewUser();
	
	@Query(value = "SELECT * FROM t_user WHERE count_product != 0 ORDER BY wedding_date", nativeQuery = true)
	List<User> findRestUser();

}
