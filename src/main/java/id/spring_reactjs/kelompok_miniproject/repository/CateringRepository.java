package id.spring_reactjs.kelompok_miniproject.repository;

import id.spring_reactjs.kelompok_miniproject.model.entity.Catering;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CateringRepository extends JpaRepository<Catering, Integer> {

	Catering findAllByIdCatering(Integer idCatering);

	@Query(value = "SELECT * FROM catering WHERE name_catering = ?1", nativeQuery = true)
	Catering getByName(String nameCatering);

	List<Catering> findAllByNameCateringAndPriceCatering(String name, Integer price);
	
	List<Catering> findAllByOrderByPriceCateringAsc();
	
	List<Catering> findAllByOrderByPriceCateringDesc();
	
	@Query(value = "SELECT id_catering FROM catering", nativeQuery = true)
	List<Object> getId();
	
	@Query(value = "SELECT name_catering FROM catering", nativeQuery = true)
	List<Object> getName();
	
	List<Catering> findAllByNameCateringIgnoreCaseContaining(String name);
	
	List<Catering> findAllByStyleCatering(String styleCatering);
	
	@Query(value = "SELECT * FROM catering WHERE portion_catering BETWEEN ?1 AND ?2", nativeQuery = true)
	List<Catering> findAllBetweenPortion(Integer min, Integer max);

}