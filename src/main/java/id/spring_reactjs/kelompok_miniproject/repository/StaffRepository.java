package id.spring_reactjs.kelompok_miniproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.spring_reactjs.kelompok_miniproject.model.entity.Assignment;
import id.spring_reactjs.kelompok_miniproject.model.entity.Photo;
import id.spring_reactjs.kelompok_miniproject.model.entity.Staff;

@Repository
public interface StaffRepository extends JpaRepository<Staff, Integer>{ 
	
	@Query(value = "SELECT s.id_staff, s.name_staff, s.job, s.profile, a.staff_schedule "
			+ "FROM staff s LEFT JOIN assignment a ON s.id_staff = a.id_staff WHERE a.id_staff ISNULL "
			+ "UNION ALL " 
			+ "(SELECT s.id_staff, s.name_staff, s.job, s.profile, MAX(a.staff_schedule) "
			+ "FROM staff s INNER JOIN assignment a ON s.id_staff = a.id_staff " 
			+ "GROUP BY s.id_staff, s.name_staff, s.job, s.profile ORDER BY MAX(a.staff_schedule))", nativeQuery = true)
	List<Staff> findLastStaffSchedule();
	
	@Query(value = "SELECT s.id_staff, s.name_staff, s.job, s.profile, a.staff_schedule "
			+ "FROM staff s LEFT JOIN assignment a ON s.id_staff = a.id_staff WHERE a.id_staff ISNULL and s.job = ?1 "
			+ "UNION ALL " 
			+ "(SELECT s.id_staff, s.name_staff, s.job, s.profile, MAX(a.staff_schedule) "
			+ "FROM staff s INNER JOIN assignment a ON s.id_staff = a.id_staff WHERE s.job = ?1 " 
			+ "GROUP BY s.id_staff, s.name_staff, s.job, s.profile ORDER BY MAX(a.staff_schedule))", nativeQuery = true)
	List<Staff> findLastStaffScheduleByJob(String job);
	
	List<Staff> findAllByJob(String job);
	List<Staff> findAllByNameStaffIgnoreCaseContaining(String name);
}
