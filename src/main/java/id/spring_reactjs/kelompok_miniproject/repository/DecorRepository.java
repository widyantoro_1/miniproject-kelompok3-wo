package id.spring_reactjs.kelompok_miniproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import id.spring_reactjs.kelompok_miniproject.model.entity.Decor;
import id.spring_reactjs.kelompok_miniproject.model.entity.Venue;

public interface DecorRepository extends JpaRepository<Decor, Integer>  {
	Decor findAllByIdDecor(Integer idDecor);

	@Query(value="SELECT * FROM decor WHERE style_decor= ?1", nativeQuery = true)
	List<Decor> findByStyleDecor(String styleDecor);
	
	Decor findByNameDecor(String nameDecor);
	
	List<Decor> findAllByNameDecorIgnoreCaseContaining(String name);
	
	@Query(value = "SELECT id_decor FROM decor", nativeQuery = true)
	List<Object> getId();
	
	@Query(value = "SELECT name_decor FROM decor", nativeQuery = true)
	List<Object> getName();
}
