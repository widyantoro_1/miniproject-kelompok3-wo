package id.spring_reactjs.kelompok_miniproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import id.spring_reactjs.kelompok_miniproject.model.entity.Admin;
import id.spring_reactjs.kelompok_miniproject.model.entity.Venue;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Integer> {
	Admin findByUnameAdminLike(String uname);
	
	List<Admin> findByUnameAdmin(String uname);
	
	Admin findByPassAdminLike(String pass);
	
	Admin findByIdAdmin(Integer idAdmin);

	
}
