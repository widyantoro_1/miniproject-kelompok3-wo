package id.spring_reactjs.kelompok_miniproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import id.spring_reactjs.kelompok_miniproject.model.entity.Venue;

@Repository
public interface VenueRepository extends JpaRepository<Venue, Integer> {
	/* select * from venue where location = "" */
	List<Venue> findAllByLocation(String loc);

	/* select * from venue where capacity = "" */
	List<Venue> findAllByCapacity(Integer cap);

	/* select * from venue where ambiance = "" */
	List<Venue> findAllByAmbiance(String amb);

	/*
	 * select * from venue where location = ?loc and capacity = ?cap
	 */
	List<Venue> findAllByLocationAndCapacity(String loc, Integer cap);

	/*
	 * select * from venue where location = ?loc and ambiance = ?amb
	 */
	List<Venue> findAllByLocationAndAmbiance(String loc, String amb);

	/*
	 * select * from venue where ambiance = ?amb and capacity = ?cap
	 */
	List<Venue> findAllByAmbianceAndCapacity(String amb, Integer cap);

	/*
	 * select * from venue where location = ?loc and capacity = ?cap and ambiance =
	 * ?amb
	 */
	List<Venue> findAllByLocationAndCapacityAndAmbiance(String loc, Integer cap, String amb);
	
	Venue findByIdVenue(Integer idVenue);
	
	Venue findByNameVenue(String nameVenue);
	
	List<Venue> findAllByNameVenueIgnoreCaseContaining(String name);
	
	@Query(value = "SELECT id_venue FROM venue", nativeQuery = true)
	List<Object> getId();
	
	@Query(value = "SELECT name_venue FROM venue", nativeQuery = true)
	List<Object> getName();

}
