package id.spring_reactjs.kelompok_miniproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.spring_reactjs.kelompok_miniproject.model.entity.Dam;

@Repository
public interface DamRepository extends JpaRepository<Dam, Integer> {
    Dam findByIdDam(Integer idDam);
    
	Dam findByNameDam(String nameDam);
	
    List<Dam> findAllByStyleDam(String styleDam);
    
    List<Dam> findAllByOrderByPriceDamAsc();
    
    List<Dam> findAllByOrderByPriceDamDesc();
    
    List<Dam> findAllByStyleDamOrderByPriceDamAsc(String styleDam);

    List<Dam> findAllByStyleDamOrderByPriceDamDesc(String styleDam);
    
	List<Dam> findAllByNameDamIgnoreCaseContaining(String nameDam);
    
    @Query(value = "SELECT id_dam FROM dam", nativeQuery = true)
	List<Object> getId();
    
    @Query(value = "SELECT name_dam FROM dam", nativeQuery = true)
	List<Object> getName();
    
//	List<Dam> findAllByNameDam(String nameDam);
//	
//    @Query(value="SELECT *" + 
//            " FROM dam" + 
//            " WHERE style_dam= ?1", nativeQuery = true)
//    List<Dam> findByStyleDam(String styleDam);
//    
//    @Query(value="SELECT *" + 
//    		"	FROM public.dam" + 
//    		"	ORDER BY price_dam", nativeQuery = true)
//    List<Dam> sortByPriceAsc();
//    
//    @Query(value="SELECT *" + 
//    		"	FROM public.dam" + 
//    		"	ORDER BY price_dam DESC", nativeQuery = true)
//    List<Dam> sortByPriceDesc();
//    
//    @Query(value="SELECT *" + 
//    		"	FROM public.dam" + 
//    		"   WHERE style_dam= ?1" +
//    		"	ORDER BY price_dam ASC", nativeQuery = true)
//    List<Dam> sortByPriceAscStyle(String styleDam);
//    
//    @Query(value="SELECT *" + 
//    		"	FROM public.dam" + 
//    		"   WHERE style_dam= ?1" +
//    		"	ORDER BY price_dam DESC", nativeQuery = true)
//    List<Dam> sortByPriceDescStyle(String styleDam);
}
