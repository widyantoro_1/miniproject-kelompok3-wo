package id.spring_reactjs.kelompok_miniproject.repository;

import id.spring_reactjs.kelompok_miniproject.model.entity.Assignment;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AssignmentRepository extends JpaRepository<Assignment, Integer> {

//	@Query(value = "SELECT a.id_assignment, s.id_staff, s.name_staff, s.job, a.id_user, a.staff_schedule "
//			+ "FROM staff s LEFT JOIN assignment a ON s.id_staff = a.id_staff WHERE a.id_staff ISNULL " 
//			+ "UNION ALL"
//			+ "(SELECT a.id_assignment, s.id_staff, s.name_staff, s.job, a.id_user, MAX(a.staff_schedule) "
//			+ "FROM staff s INNER JOIN assignment a ON s.id_staff = a.id_staff "
//			+ "GROUP BY a.id_assignment, s.id_staff, s.name_staff, s.job, a.id_user "
//			+ "ORDER BY MAX(a.staff_schedule))", nativeQuery = true)
//	List<Assignment> findLastStaffSchedule();
	
	List<Assignment> findAllByUserIdUser(Integer idUser);
	
	List<Assignment> findAllByStaffIdStaff(Integer idStaff);
	
	Assignment findByUserIdUser(Integer idUser);
}
