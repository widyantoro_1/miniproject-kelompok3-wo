package id.spring_reactjs.kelompok_miniproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.spring_reactjs.kelompok_miniproject.model.entity.Photo;

@Repository
public interface PhotoRepository extends JpaRepository<Photo, Integer>{ 
	//@Query(value = 'SELECT * FROM photo WHERE name_photo=?1)
	Photo findByNamePhoto(String namePhoto);
	
	List<Photo> findAllByThemePhoto(String themePhoto);
	
	List<Photo> findAllByOrderByPricePhotoAsc();
	
	List<Photo> findAllByOrderByPricePhotoDesc();
	
	List<Photo> findAllByThemePhotoOrderByPricePhotoAsc(String themePhoto);
	
	List<Photo> findAllByThemePhotoOrderByPricePhotoDesc(String themePhoto);
	
	Photo findByIdPhoto(Integer id);
	
	@Query(value = "SELECT id_photo FROM photo", nativeQuery = true)
	List<Object> getId();
	
	@Query(value = "SELECT name_photo FROM photo", nativeQuery = true)
	List<Object> getName();
	
    List<Photo> findAllByNamePhotoIgnoreCaseContaining(String name);
}
