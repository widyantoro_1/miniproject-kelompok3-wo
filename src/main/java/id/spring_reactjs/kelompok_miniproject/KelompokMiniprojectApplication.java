package id.spring_reactjs.kelompok_miniproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import id.spring_reactjs.kelompok_miniproject.property.FileStorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({ FileStorageProperties.class })
public class KelompokMiniprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(KelompokMiniprojectApplication.class, args);
	}

}
