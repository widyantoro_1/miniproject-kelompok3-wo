package id.spring_reactjs.kelompok_miniproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PhotoDto {
	private Integer idPhoto;
	private String namePhoto;
	private String imagePhoto;
	private String themePhoto;
	private Integer pricePhoto;
	private String detailPhoto;

}
