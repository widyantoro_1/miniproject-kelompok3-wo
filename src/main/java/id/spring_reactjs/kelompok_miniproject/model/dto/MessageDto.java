package id.spring_reactjs.kelompok_miniproject.model.dto;

public class MessageDto{
	private String status;
	private String message;
	private AdminDto dataAdmin;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public AdminDto getDataAdmin() {
		return dataAdmin;
	}

	public void setDataAdmin(AdminDto dataAdmin) {
		this.dataAdmin = dataAdmin;
	}

}
