package id.spring_reactjs.kelompok_miniproject.model.dto;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AssignmentDto {
	private Integer idAssignment;
	private Integer idStaff;
	private String nameStaff;
	private String job;
	private Integer idUser;
	private Date staffSchedule;
}
