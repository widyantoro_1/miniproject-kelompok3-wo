package id.spring_reactjs.kelompok_miniproject.model.dto;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VenueDto {
	private Integer idVenue;
	private String nameVenue;
	private String location;
	private String ambiance;
	private Integer capacity;
//	private byte[] imageVenue;
	private String imageVenue;
	private Integer priceVenue;
	private String detailVenue;
	private MultipartFile fileVenue;

}
