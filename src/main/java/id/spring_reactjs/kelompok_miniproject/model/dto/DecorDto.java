package id.spring_reactjs.kelompok_miniproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DecorDto {
	private Integer idDecor;
	private String nameDecor;
	private String styleDecor;
	private String imageDecor;
	private Integer priceDecor;
	private String detailDecor;
}
