package id.spring_reactjs.kelompok_miniproject.model.dto;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDto {
	private Integer idUser;
	private String nameBride;
	private String nameGroom;
	private String email;
	private String noHp;
	private Date weddingDate;
	private Integer guestNumber;
	private Integer idVenue;
	private Integer idDecor;
	private Integer idDam;
	private Integer idCatering;
	private Integer idPhoto;
	private String nameVenue;
	private String nameDecor;
	private String nameDam;
	private String nameCatering;
	private String namePhoto;
	private Integer totalPrice;
	private Integer countProduct;
	private String reference;
	private String progress;
	private Date updatedDate;
	private String admin;
//	private Venue venue;
//	private Decor decor;
//	private Dam dam;
//	private Catering catering;
//	private Photo photo;
}
