package id.spring_reactjs.kelompok_miniproject.model.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CateringDto {
	private Integer idCatering;
	private String nameCatering;
	private String styleCatering;
	private Integer portionCatering;
	private String imageCatering;
	private Integer priceCatering;
	private String detailCatering;
}
