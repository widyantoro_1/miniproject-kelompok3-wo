package id.spring_reactjs.kelompok_miniproject.model.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AdminDto {
	private Integer idAdmin;
	private String unameAdmin;
	private String nameAdmin;
	private String passAdmin;
	private String imageAdmin;

}
