package id.spring_reactjs.kelompok_miniproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DamDto {
	private Integer idDam;
    private String nameDam;
    private String styleDam;
    private String imageDam;
    private Integer priceDam;
    private String detailDam;
}
