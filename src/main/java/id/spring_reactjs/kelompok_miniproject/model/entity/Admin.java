package id.spring_reactjs.kelompok_miniproject.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "admin")
public class Admin {
	@Id
	// @GeneratedValue(generator = "uuid")
	// @GenericGenerator(name = "uuid", strategy = "uuid2")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_admin")
	private Integer idAdmin;

	@Column(name = "username_admin", unique = true)
	private String unameAdmin;
	
	@Column(name = "name_admin")
	private String nameAdmin;

	@Column(name = "password_admin")
	private String passAdmin;
	
	@Column(name = "image_admin")
	private String imageAdmin;

}
