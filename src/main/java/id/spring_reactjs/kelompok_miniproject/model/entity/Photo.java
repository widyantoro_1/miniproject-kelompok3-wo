package id.spring_reactjs.kelompok_miniproject.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "photo")
public class Photo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_photo")
	private Integer idPhoto;
	
	@Column(name = "name_photo", nullable = false, unique = true)
	private String namePhoto;
	
	@Column(name="image_photo", nullable = false)
	private String imagePhoto;
	
	@Column(name="theme_photo", nullable=false)
	private String themePhoto;
	
	@Column(name="price_photo", nullable = false)
	private Integer pricePhoto;
	
	@Column(name="detail_photo", nullable = false)
	private String detailPhoto;

}
