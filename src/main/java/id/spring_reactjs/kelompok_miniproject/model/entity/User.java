package id.spring_reactjs.kelompok_miniproject.model.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "t_user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_user")
	private Integer idUser;

	@Column(name = "name_bride")
	private String nameBride;

	@Column(name = "name_groom")
	private String nameGroom;

	@Column(name = "email")
	private String email;

	@Column(name = "no_hp")
	private String noHp;

	@Column(name = "wedding_date", unique = true)
	private Date weddingDate;

	@Column(name = "guest_number")
	private Integer guestNumber;

	@ManyToOne
	@JoinColumn(name="id_venue", insertable = false, updatable = false)
	private Venue venue;
	
	@Column(name = "id_venue")
	private Integer idVenue;

	@ManyToOne
	@JoinColumn(name="id_decor", insertable = false, updatable = false)
	private Decor decor;
	
	@Column(name = "id_decor")
	private Integer idDecor;

	@ManyToOne
	@JoinColumn(name="id_dam", insertable = false, updatable = false)
	private Dam dam;
	
	@Column(name = "id_dam")
	private Integer idDam;

	@ManyToOne
	@JoinColumn(name="id_catering", insertable = false, updatable = false)
	private Catering catering;
	
	@Column(name = "id_catering")
	private Integer idCatering;

	@ManyToOne
	@JoinColumn(name="id_photo", insertable = false, updatable = false)
	private Photo photo;
	
	@Column(name = "id_photo")
	private Integer idPhoto;

	@Column(name = "total_price")
	private Integer totalPrice;
	
	@Column(name = "count_product")
	private Integer countProduct;
	
	@Column(name = "reference")
	private String reference;
	
	@Column(name = "progress")
	private String progress;
	
	@Column(name = "updated_date")
	private Date updatedDate;
	
	@Column(name = "admin")
	private String admin;
}
