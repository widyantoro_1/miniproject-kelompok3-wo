package id.spring_reactjs.kelompok_miniproject.model.entity;

import java.sql.Date;

import javax.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "assignment")
public class Assignment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_assignment")
	private Integer idAssignment;

	@ManyToOne
	@JoinColumn(name = "id_staff", referencedColumnName = "id_staff", nullable = false)
	private Staff staff;
	
	@Column(name = "name_staff")
	private String nameStaff;
	
	@Column(name = "job")
	private String job;
	
	@ManyToOne
	@JoinColumn(name = "id_user", referencedColumnName = "id_user", nullable = false)
	private User user;
	
	@Column(name = "staff_schedule")
	private Date staffSchedule;

}
