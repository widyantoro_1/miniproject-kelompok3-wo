package id.spring_reactjs.kelompok_miniproject.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "dam")
public class Dam {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_dam")
	private Integer idDam;

	@Column(name = "name_dam", nullable = false, unique = true)
	private String nameDam;

	@Column(name = "style_dam", nullable = false)
	private String styleDam;

	@Column(name = "image_dam", nullable = false)
	private String imageDam;

	@Column(name = "price_dam", nullable = false)
	private Integer priceDam;

	@Column(name = "detail_dam", nullable = false)
	private String detailDam;

}