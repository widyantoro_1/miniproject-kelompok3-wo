package id.spring_reactjs.kelompok_miniproject.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "decor")
public class Decor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_decor")
	private Integer idDecor;

	@Column(name = "name_decor", unique = true)
	private String nameDecor;

	@Column(name = "style_decor", nullable = false)
	private String styleDecor;
 
	@Column(name = "image_decor", nullable=true)
	private String imageDecor;

	@Column(name = "price_decor", nullable = true)
	private Integer priceDecor;

	@Column(name = "detail_decor", nullable = true)
	private String detailDecor;
	

}