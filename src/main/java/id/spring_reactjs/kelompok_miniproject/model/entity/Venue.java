package id.spring_reactjs.kelompok_miniproject.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "venue")
public class Venue {

	@Id
	// @GeneratedValue(generator = "uuid")
	// @GenericGenerator(name = "uuid", strategy = "uuid2")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_venue")
	private Integer idVenue;

	@Column(name = "name_venue", unique = true)
	private String nameVenue;

	@Column(name = "location")
	private String location;

	@Column(name = "ambiance")
	private String ambiance;

	@Column(name = "capacity")
	private Integer capacity;

//	@Lob
//	@Column(name = "image_venue")
//	private byte[] imageVenue;
	
	@Column(name = "image_venue")
	private String imageVenue;

	@Column(name = "price_venue")
	private Integer priceVenue;

	@Column(name = "detail_venue")
	private String detailVenue;

}
