package id.spring_reactjs.kelompok_miniproject.model.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="staff")
public class Staff {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_staff")
	private Integer idStaff;
	
	@Column(name="name_staff", nullable = false)
	private String nameStaff;
	
	@Column(name="job", nullable = false)
	private String job;
	
	@Column(name="profile", nullable = false)
	private String profile;
	
	@Column(name = "staff_schedule")
	private Date staffSchedule;

}
