package id.spring_reactjs.kelompok_miniproject.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "catering")
public class Catering {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_catering")
	private Integer idCatering;
	
	@Column(name = "name_catering", unique = true)
	private String nameCatering;
	
	@Column(name = "style_catering", nullable = false)
	private String styleCatering;
	
	@Column(name = "portion_catering", nullable = false)
	private Integer portionCatering;
	
	@Column(name = "image_catering", nullable = false)
	private String imageCatering;
	
	@Column(name = "price_catering", nullable = false)
	private Integer priceCatering;
	
	@Column(name = "detail_catering", nullable = false)
	private String detailCatering;
	
}
